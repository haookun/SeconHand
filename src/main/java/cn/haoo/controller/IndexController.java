package cn.haoo.controller;

import cn.haoo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    @Autowired
    //真实访问地址：项目名/hello
    @RequestMapping("/")
    // 不走视图解析器
    //@ResponseBody
    public String index(){
        //封装数据,可以在jsp页面取出并渲染
        System.out.println("现在进入index.jsp");
        return "redirect:index"; //会被视图解析器处理
    }
}
