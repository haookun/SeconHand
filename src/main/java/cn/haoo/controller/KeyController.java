package cn.haoo.controller;

import cn.haoo.pojo.IndexView;
import cn.haoo.pojo.Item;
import cn.haoo.pojo.Key;
import cn.haoo.pojo.User;
import cn.haoo.service.KeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/user/key")
public class KeyController {

    @Autowired
    KeyService keyService;


    @RequestMapping("")
    public String toKey(Model model, HttpSession session){
        User user= (User) session.getAttribute("user");
        List<Key> keys=keyService.selectByUid(user.getUid());
        model.addAttribute("key",keys);
        return "/user/key";
    }

    @RequestMapping("/delete")
    public String deleteKey(Model model,int kid){
        keyService.deleteByPrimaryKey(kid);
        return "redirect:/user/key";
    }

    @RequestMapping("/insert")
    public String insertKey(Model model,HttpSession session,Key key){
        User user= (User) session.getAttribute("user");
        List<Key> keys=keyService.selectByUid(user.getUid());
        System.out.println(keys.contains(key));
        if(!keys.contains(key)){
            keyService.insertSelective(key);
        }
        return "redirect:/user/key";
    }

    @RequestMapping("/update")
    public String updateKey(Model model,HttpSession session,Key key){
        User user= (User) session.getAttribute("user");
        List<Key> keys=keyService.selectByUid(user.getUid());
        System.out.println(keys.contains(key));
        if(!keys.contains(key)){
            keyService.updateByPrimaryKeySelective(key);
        }
        return "redirect:/user/key";
    }


}
