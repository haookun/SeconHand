package cn.haoo.controller;

import cn.haoo.pojo.User;
import cn.haoo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class HelloController {


    @Autowired
/*    @Qualifier("UserServiceImpl")//对应spring-service.xml*/
    private UserService userService;
    //真实访问地址：项目名/hello
    @RequestMapping("/hello")
    // 不走视图解析器
    //@ResponseBody
    public String hello(Model model){
        //封装数据,可以在jsp页面取出并渲染
        System.out.println("现在进入index.jsp???");
        User user=userService.selectByPrimaryKey(1);
        if(user!=null){
            String s=user.toString();
            System.out.println(s);
            System.out.println("结束");
        }
        System.out.println("结束2");
        model.addAttribute("mas","Hello,Spring");
        return "index"; //会被视图解析器处理
    }
}