package cn.haoo.controller;

import cn.haoo.pojo.*;
import cn.haoo.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

import static cn.haoo.controller.ItemEidtController.strToDateLong;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private ItemService itemService;
    @Autowired
    private TopService topService;
    @Autowired
    private UserService userService;
    @Autowired
    private GreatService greatService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private  CommentreadService commentreadService;
    @Autowired
    private KeyService keyService;
    @Autowired
    private HandselServer handselServer;

    @RequestMapping("")
    public String toAdmin(Model model, HttpSession session) {
        User user = (User) session.getAttribute("user");
        if (user.getUauth() != 1)
            return "redirect:/index";
        return "/admin/admin";
    }

    @RequestMapping("/item")
    public String toAdminItem(Model model) {
        List<Item> list = itemService.selectAll();
        model.addAttribute("list", ShowIndexView(list));
        return "/admin/item";
    }

    @RequestMapping("/item/delete")
    public String deleteItem(Model model, HttpSession session, int iid) {
        Item item = itemService.selectByPrimaryKey(iid);
        User user = (User) session.getAttribute("user");
        if (!item.getUid().equals(user.getUid())) {
            return "redirect:/item/detail?iid=" + item.getIid();
        }
        System.out.println("删除了" + item.toString());
        itemService.deleteByPrimaryKey(item.getIid());
        return "redirect:/admin/item";
    }

    @RequestMapping("/item/top")
    public String toAdminItemTop(Model model) {
        List<Item> toplist = itemService.selectTop();
        model.addAttribute("toplist", ShowIndexView(toplist));
        return "/admin/itemtop";
    }

    @RequestMapping("/item/top/to")
    public String toTopItem(Model model, int iid) {
        Top top=new Top();
        if(topService.selectByIid(iid)==null) {
            top.setIid(iid);
            topService.insertSelective(top);
            List<Item> toplist = itemService.selectTop();
            model.addAttribute("toplist", ShowIndexView(toplist));
        }
        return "redirect:/admin/item";
    }

    @RequestMapping("/item/top/delete")
    public String deleteTopItem(Model model, int iid) {
        topService.deleteByIid(iid);
        List<Item> toplist = itemService.selectTop();
        model.addAttribute("toplist", ShowIndexView(toplist));
        return "redirect:/admin/item/top";
    }

    @RequestMapping("/user")
    public String toAdminUser(Model model) {
        List<User> userList=userService.selectAll();
        model.addAttribute("userlist", userList);
        return "/admin/user";
    }

    @RequestMapping("/comm")
    public String toAdminComm(Model model) {
        List<Commentread> commentList =commentreadService.selectALl();
        model.addAttribute("list", commentList);
        return "/admin/comm";
    }

    /*从数据库中拿出index需要列出的item信息*/
    public List<IndexView> ShowIndexView(List<Item> list) {
        List<IndexView> listview = new ArrayList<IndexView>();
        for (int i = 0; i < list.size(); i++) {
            //开始循环
            Item item = list.get(i);
            //list中添加
            listview.add(ShowItemView(item));
        }
        return listview;
    }

    /*从数据库中拿出detail需要列出的item信息*/
    public IndexView ShowItemView(Item item) {
        IndexView itemview = new IndexView();
        itemview.setIndexViewInAdmin(item, userService.selectByPrimaryKey(item.getUid()),
                greatService.selectByIid(item.getIid()), commentService.selectByIid(item.getIid()));
        return itemview;
    }
}
