package cn.haoo.controller;

import cn.haoo.pojo.Comment;
import cn.haoo.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/comm")
public class CommentController {

    @Autowired
    private CommentService commentService;

    //评论
    @PostMapping("/reply")
    public String commforItem(Model model, Comment comment){
        commentService.insertSelective(comment);
        System.out.println("评论："+comment.toString());
        return "redirect:/item/detail?iid="+comment.getIid();
    }


}
