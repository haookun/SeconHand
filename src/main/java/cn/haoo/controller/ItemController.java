package cn.haoo.controller;


import cn.haoo.dao.ItemMapper;
import cn.haoo.dao.TopMapper;
import cn.haoo.pojo.*;
import cn.haoo.service.*;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class ItemController {

    @Autowired
    private ItemService itemService;
    @Autowired
    private TopService topService;
    @Autowired
    private UserService userService;
    @Autowired
    private GreatService greatService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private KeyService keyService;
    @Autowired
    private HandselServer handselServer;

    //真实访问地址：项目名/hello
    @RequestMapping("/index")
    // 不走视图解析器
    /*@ResponseBody*/
    public String FirstPage(Model model, HttpSession session) {
        //封装数据,可以在jsp页面取出并渲染
        System.out.println("开始处理index的数据");
        List<Item> list = itemService.selectAll();
        System.out.println("这是list" + list);
        List<Item> toplist = itemService.selectTop();
        System.out.println("这是top" + toplist);
        model.addAttribute("toplist", ShowIndexView(toplist));
        model.addAttribute("list", ShowIndexView(list));
        model.addAttribute("hotuser", userService.selectByHot());
        model.addAttribute("hotitem", itemService.selectByHot());
        session.removeAttribute("itype");
        return "index"; //会被视图解析器处理
    }

    @RequestMapping("/search")
    public String serchPage(Model model, String iname) {
        List<Item> list = itemService.selectByIname(iname);
        System.out.println("这是list" + list);
        model.addAttribute("list", ShowIndexView(list));
        return "index"; //会被视图解析器处理
    }

    @RequestMapping("/focus")
    public String foucusPage(Model model, HttpSession session) {
        User user = (User) session.getAttribute("user");
        List<Key> keys = keyService.selectByUid(user.getUid());
        List<Item> items = new ArrayList<Item>();
        for (int i = 0; i < keys.size(); i++) {
            Key key = keys.get(i);
            items.addAll(itemService.selectByIname(key.getKword()));
        }
        model.addAttribute("list", ShowIndexView(items));
        return "index";
    }

    @RequestMapping("/index/type")
    public String typePage(Model model, HttpSession session, String type) {
        int itype = 1;
        List<Item> list = new ArrayList<Item>();
        System.out.println(type);
        String types[] = {"公告", "图文", "未结束", "已结束", "二手", "捐赠", "热议"};
        while (!types[itype - 1].equals(type) && itype < 9) {
            itype++;
        }
        System.out.println(itype);

        if (itype <= 4)
            list = itemService.selectByItype(itype);
        else if (itype == 5)
            list = itemService.selectSeconHand();
        else if (itype == 6) {
            Date day = new Date();
            List<Handsel> handsels = handselServer.selectByhtime(day);
            for (int i = 0; i < handsels.size(); i++) {
                Handsel handsel = handsels.get(i);
                list.add(itemService.selectByPrimaryKey(handsel.getIid()));
            }
        } else
            list = itemService.selectAll();
        List<IndexView> indexViewList = ShowIndexView(list);
        if (itype == 7) {
            Collections.sort(indexViewList);
            System.out.println(indexViewList.toString());
        }
        model.addAttribute("list", indexViewList);
        session.setAttribute("itype", itype);
        model.addAttribute("hotuser", userService.selectByHot());
        model.addAttribute("hotitem", itemService.selectByHot());
        return "index";
    }

    /*从数据库中拿出index需要列出的item信息*/
    public List<IndexView> ShowIndexView(List<Item> list) {
        List<IndexView> listview = new ArrayList<IndexView>();
        for (int i = 0; i < list.size(); i++) {
            //开始循环
            Item item = list.get(i);
            //list中添加
            listview.add(ShowItemView(item));
        }
        return listview;
    }

    /*从数据库中拿出detail需要列出的item信息*/
    public IndexView ShowItemView(Item item) {
        IndexView itemview = new IndexView();
        itemview.setIndexView(item, userService.selectByPrimaryKey(item.getUid()),
                greatService.selectByIid(item.getIid()), commentService.selectByIid(item.getIid()));
        return itemview;
    }

}
