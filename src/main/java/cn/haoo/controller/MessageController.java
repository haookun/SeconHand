package cn.haoo.controller;

import cn.haoo.pojo.*;
import cn.haoo.service.*;
import com.alibaba.fastjson.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/user/message")
public class MessageController {

    @Autowired
    private UserService userService;
    @Autowired
    private CommentService commentService;
    @Autowired
    private ItemService itemService;
    @Autowired
    private GreatService greatService;
    @Autowired
    private CommentreadService commentreadService;
    @Autowired
    private GreatreadService greatreadService;

    /*我的消息*/
    /*需要：
     * 评论者 评论了 你 的 被评论者的item ：评论内容（评论者user 被评论的itemp 评论的comm）
     * 评论者 评论了 你 的 被评论者的comm ：评论内容（评论者user 被评论的commp 评论的comm）
     * 点赞者 攒了 你 的 被赞者的item/comm（赞者user 被赞userp ）
     * */
    @RequestMapping("")
    public String massageUser(Model model, HttpSession session) {
        User userp = (User) session.getAttribute("user");
        List<Commentread> commentreads = commentreadService.selectByUidp(userp.getUid());
        List<Greatread> greatreads = greatreadService.selectByUipd(userp.getUid());
        //System.out.println(commentreads);
        //System.out.println(greatreads);
        model.addAttribute("commentreads", commentreads);
        model.addAttribute("greatreads", greatreads);
        return "/user/message";
    }


    //未读消息数量
    @RequestMapping("/nums")
    public Great messageNum() {
        System.out.println("未读消息");
        Great great = null;
        return great;
    }


    //消息已读
    @RequestMapping("/read")
    public String messageRead(Model model, int cid, int gid) {
        if (cid != -1) {
            Comment comment = commentService.selectByPrimaryKey(cid);
            comment.setCread(1);
            System.out.println(comment);
            commentService.updateByPrimaryKey(comment);
        }
        if (gid != -1) {
            Great great = greatService.selectByPrimaryKey(gid);
            great.setGread(1);
            System.out.println(great);
            greatService.updateByPrimaryKey(great);
        }
        System.out.println("消息已读");
        return "redirect:/user/message";
    }

    //消息全部已读
    @RequestMapping("/readall")
    public String messageReadAll(Model model, HttpSession session) {
        User userp = (User) session.getAttribute("user");
        List<Commentread> commentreads = commentreadService.selectByUidp(userp.getUid());
        List<Greatread> greatreads = greatreadService.selectByUipd(userp.getUid());
        for (int i = 0; i < commentreads.size(); i++) {
            //开始循环
            Commentread commentread = commentreads.get(i);
            Comment comment = commentService.selectByPrimaryKey(commentread.getCid());
            comment.setCread(1);
            commentService.updateByPrimaryKeySelective(comment);
        }
        for (int i = 0; i < greatreads.size(); i++) {
            //开始循环
            Greatread greatread = greatreads.get(i);
            Great great = greatService.selectByPrimaryKey(greatread.getGid());
            great.setGread(1);
            greatService.updateByPrimaryKeySelective(great);
        }
        System.out.println("消息全部已读");
        return "redirect:/user/message";
    }



}
