package cn.haoo.pojo;

import lombok.Data;

@Data
public class Top {
    private Integer tid;

    private Integer iid;

    public Top(Integer tid, Integer iid) {
        this.tid = tid;
        this.iid = iid;
    }

    public Top() {
        super();
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public Integer getIid() {
        return iid;
    }

    public void setIid(Integer iid) {
        this.iid = iid;
    }
}