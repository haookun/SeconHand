package cn.haoo.pojo;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class IndexView implements Comparable<IndexView>{
    /*用户信息*/
    private Integer uid;

    private String uname;

    private Integer uauth;

    /*帖信息*/
    private Integer iid;

    private String iname;

    private Integer itype;

    private String itime;

    private Integer ibrowse;

    /*赞*/
    private Integer great;

    /*评论*/
    private Integer comment;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public Integer getUauth() {
        return uauth;
    }

    public void setUauth(Integer uauth) {
        this.uauth = uauth;
    }

    public Integer getIid() {
        return iid;
    }

    public void setIid(Integer iid) {
        this.iid = iid;
    }

    public String getIname() {
        return iname;
    }

    public void setIname(String iname) {
        this.iname = iname;
    }

    public Integer getItype() {
        return itype;
    }

    public void setItype(Integer itype) {
        this.itype = itype;
    }

    public String getItime() {
        return itime;
    }

    public void setItime(Date oldtime) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss");
        Date today = new Date(System.currentTimeMillis());
        /*时间差*/
        long to = today.getTime();
        long from = oldtime.getTime();
        String mark[] = {"分钟", "小时", "天"};
        int time = (int) ((to - from) / (1000 * 60));
        int i = 0;
        for (i = 0; i < 3; i++) {
            if (i == 0 && time >= 60)
                time = time / 60;
            else if (i == 1 && time >= 24)
                time = time / 24;
            else break;
        }
        System.out.println("和现在差了：" + time + mark[i] + i);
        String itime = (time + mark[i]);
        this.itime = itime;
    }

    public Integer getIbrowse() {
        return ibrowse;
    }

    public void setIbrowse(Integer ibrowse) {
        this.ibrowse = ibrowse;
    }

    public Integer getGreat() {
        return great;
    }

    public void setGreat(Integer great) {
        this.great = great;
    }

    public Integer getComment() {
        return comment;
    }

    public void setComment(Integer comment) {
        this.comment = comment;
    }

    public void setIndexView(Item item, User user, Integer gnum, Integer cnum) {
        setComment(cnum);
        setGreat(gnum);
        setIbrowse(item.getIbrowse());
        setIid(item.getIid());
        setIname(item.getIname());
        setItime(item.getItime());
        setItype(item.getItype());
        setUauth(user.getUauth());
        setUid(user.getUid());
        setUname(user.getUname());
    }

    public void setIndexViewInAdmin(Item item, User user, Integer gnum, Integer cnum) {
        setComment(cnum);
        setGreat(gnum);
        setIbrowse(item.getIbrowse());
        setIid(item.getIid());
        setIname(item.getIname());
        this.itime=item.getChangedItime();
        setItype(item.getItype());
        setUauth(user.getUauth());
        setUid(user.getUid());
        setUname(user.getUname());
    }

    @Override
    public String toString() {
        return "IndexView{" +
                "uid=" + uid +
                ", uname='" + uname + '\'' +
                ", uauth=" + uauth +
                ", iid=" + iid +
                ", iname='" + iname + '\'' +
                ", itype=" + itype +
                ", itime='" + itime + '\'' +
                ", ibrowse=" + ibrowse +
                ", great=" + great +
                ", comment=" + comment +
                '}';
    }

    @Override
    public int compareTo(IndexView o) {
        int i = (this.getIbrowse() + this.getComment() + this.getGreat()) - (o.getIbrowse() + o.getComment() + o.getGreat());
            return -i;
    }

    public String timeToNow(Date oldtime) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss");
        Date today = new Date(System.currentTimeMillis());
        /*时间差*/
        long to = today.getTime();
        long from = oldtime.getTime();
        String mark[] = {"分钟", "小时", "天"};
        int time = (int) ((to - from) / (1000 * 60));
        int i = 0;
        for (i = 0; i < 3; i++) {
            if (i == 0 && time >= 60)
                time = time / 60;
            else if (i == 1 && time >= 24)
                time = time / 24;
            else break;
        }
        String timetonow = (time + mark[i]);
        return timetonow;
    }
}
