package cn.haoo.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Great {
    private Integer gid;

    private Integer iid;

    private Integer cid;

    private Integer uid;

    private Date gtime;

    private Integer gread;

    public Great(Integer gid, Integer iid, Integer cid, Integer uid, Date gtime, Integer gread) {
        this.gid = gid;
        this.iid = iid;
        this.cid = cid;
        this.uid = uid;
        this.gtime = gtime;
        this.gread = gread;
    }

    public Great() {
        super();
    }

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public Integer getIid() {
        return iid;
    }

    public void setIid(Integer iid) {
        this.iid = iid;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Date getGtime() {
        return gtime;
    }

    public void setGtime(Date gtime) {
        this.gtime = gtime;
    }

    public Integer getGread() {
        return gread;
    }

    public void setGread(Integer gread) {
        this.gread = gread;
    }
}