package cn.haoo.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class User {
    private Integer uid;

    private String uname;

    private String uno;

    private String upass;

    private String usex;

    private String utel;

    private Integer uauth;

    private String uother;

    public User(Integer uid, String uname, String uno, String upass, String usex, String utel, Integer uauth, String uother) {
        this.uid = uid;
        this.uname = uname;
        this.uno = uno;
        this.upass = upass;
        this.usex = usex;
        this.utel = utel;
        this.uauth = uauth;
        this.uother = uother;
    }

    public User() {
        super();
    }

    public User(String uname, String uno, String upass, String usex, String utel) {
        User user = new User();
        user.setUno(uno);
        user.setUname(uname);
        user.setUpass(upass);
        if (usex == "m") {
            user.setUpass("男");
        } else if (usex == "w") {
            user.setUsex("女");
        }
        user.setUtel(utel);
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname == null ? null : uname.trim();
    }

    public String getUno() {
        return uno;
    }

    public void setUno(String uno) {
        this.uno = uno == null ? null : uno.trim();
    }

    public String getUpass() {
        return upass;
    }

    public void setUpass(String upass) {
        this.upass = upass == null ? null : upass.trim();
    }

    public String getUsex() {
        return usex;
    }

    public void setUsex(String usex) {
        this.usex = usex == null ? null : usex.trim();
    }

    public String getUtel() {
        return utel;
    }

    public void setUtel(String utel) {
        this.utel = utel == null ? null : utel.trim();
    }

    public Integer getUauth() {
        return uauth;
    }

    public void setUauth(Integer uauth) {
        this.uauth = uauth;
    }

    public String getUother() {
        return uother;
    }

    public void setUother(String uother) {
        this.uother = uother == null ? null : uother.trim();
    }



    public String toString() {
        return "User{" +
                "uid=" + uid +
                ", uname='" + uname + '\'' +
                ", uno='" + uno + '\'' +
                ", upass='" + upass + '\'' +
                ", usex='" + usex + '\'' +
                ", utel='" + utel + '\'' +
                ", uauth=" + uauth +
                ", uother='" + uother + '\'' +
                '}';
    }
}