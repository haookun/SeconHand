package cn.haoo.pojo;

import lombok.Data;

@Data
public class Key {
    private Integer kid;

    private String kword;

    private Integer uid;

    public Key(Integer kid, String kword, Integer uid) {
        this.kid = kid;
        this.kword = kword;
        this.uid = uid;
    }

    public Key() {
        super();
    }

    public Integer getKid() {
        return kid;
    }

    public void setKid(Integer kid) {
        this.kid = kid;
    }

    public String getKword() {
        return kword;
    }

    public void setKword(String kword) {
        this.kword = kword == null ? null : kword.trim();
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }
}