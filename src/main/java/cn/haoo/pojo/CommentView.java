package cn.haoo.pojo;

import lombok.Data;

@Data
public class CommentView {
    private Comment comment;
    private Comment commentp;
    private User user;
    private User userp;
    private int great;

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public Comment getCommentp() {
        return commentp;
    }

    public void setCommentp(Comment commentp) {
        this.commentp = commentp;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUserp() {
        return userp;
    }

    public void setUserp(User userp) {
        this.userp = userp;
    }

    public int getGreat() {
        return great;
    }

    public void setGreat(int great) {
        this.great = great;
    }
}
