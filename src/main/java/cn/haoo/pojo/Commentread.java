package cn.haoo.pojo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.Data;

/**
 * commentread
 * @author 
 */
@Data
public class Commentread implements Serializable {
    /**
     * 评论id（主键）自动分配
     */
    private Integer cid;

    /**
     * 用户id（外键）
     */
    private Integer uid;

    /**
     * 用户名，不允许重复
     */
    private String uname;

    /**
     * 评论内容
     */
    private String ccontent;

    /**
     * 评论时间
     */
    private Date ctime;

    /**
     * 被读状态（已读1，未读0（默认））
     */
    private Integer cread;

    /**
     * 帖编号（主键）自动分配
     */
    private Integer iidp;

    /**
     * 发帖用户编号（外键）
     */
    private Integer iuidp;

    /**
     * 帖标题，长度限制在20字以内
     */
    private String iname;

    /**
     * 用户名，不允许重复
     */
    private String iunamep;

    /**
     * 评论id（主键）自动分配
     */
    private Integer cidp;

    /**
     * 用户id（外键）
     */
    private Integer cuidp;

    /**
     * 用户名，不允许重复
     */
    private String cunamep;

    /**
     * 评论内容
     */
    private String ccontentp;

    private static final long serialVersionUID = 1L;

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getCcontent() {
        return ccontent;
    }

    public void setCcontent(String ccontent) {
        this.ccontent = ccontent;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public Integer getCread() {
        return cread;
    }

    public void setCread(Integer cread) {
        this.cread = cread;
    }

    public Integer getIidp() {
        return iidp;
    }

    public void setIidp(Integer iidp) {
        this.iidp = iidp;
    }

    public Integer getIuidp() {
        return iuidp;
    }

    public void setIuidp(Integer iuidp) {
        this.iuidp = iuidp;
    }

    public String getIname() {
        return iname;
    }

    public void setIname(String iname) {
        this.iname = iname;
    }

    public String getIunamep() {
        return iunamep;
    }

    public void setIunamep(String iunamep) {
        this.iunamep = iunamep;
    }

    public Integer getCidp() {
        return cidp;
    }

    public void setCidp(Integer cidp) {
        this.cidp = cidp;
    }

    public Integer getCuidp() {
        return cuidp;
    }

    public void setCuidp(Integer cuidp) {
        this.cuidp = cuidp;
    }

    public String getCunamep() {
        return cunamep;
    }

    public void setCunamep(String cunamep) {
        this.cunamep = cunamep;
    }

    public String getCcontentp() {
        return ccontentp;
    }

    public void setCcontentp(String ccontentp) {
        this.ccontentp = ccontentp;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String timeToNow(Date oldtime) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss");
        Date today = new Date(System.currentTimeMillis());
        /*时间差*/
        long to = today.getTime();
        long from = oldtime.getTime();
        String mark[] = {"分钟", "小时", "天"};
        int time = (int) ((to - from) / (1000 * 60));
        int i = 0;
        for (i = 0; i < 3; i++) {
            if (i == 0 && time >= 60)
                time = time / 60;
            else if (i == 1 && time >= 24)
                time = time / 24;
            else break;
        }
        String timetonow = (time + mark[i]);
        return timetonow;
    }
}