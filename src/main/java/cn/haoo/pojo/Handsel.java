package cn.haoo.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class Handsel {
    private Integer hid;

    private Integer iid;

    private Date htime;

    public Handsel(Integer hid, Integer iid, Date htime) {
        this.hid = hid;
        this.iid = iid;
        this.htime = htime;
    }

    public Handsel() {
        super();
    }

    public Integer getHid() {
        return hid;
    }

    public void setHid(Integer hid) {
        this.hid = hid;
    }

    public Integer getIid() {
        return iid;
    }

    public void setIid(Integer iid) {
        this.iid = iid;
    }

    public Date getHtime() {
        return htime;
    }

    public void setHtime(Date htime) {
        this.htime = htime;
    }
}