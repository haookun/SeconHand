package cn.haoo.pojo;

import lombok.Data;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

@Data
public class Item {
    private Integer iid;

    private Integer uid;

    private String iname;

    private Integer iimage;

    private Integer itype;

    private Date itime;

    private Integer ibrowse;

    private String iintro;

    public Item(Integer iid, Integer uid, String iname, Integer iimage, Integer itype, Date itime, Integer ibrowse, String iintro) {
        this.iid = iid;
        this.uid = uid;
        this.iname = iname;
        this.iimage = iimage;
        this.itype = itype;
        this.itime = itime;
        this.ibrowse = ibrowse;
        this.iintro = iintro;
    }

    public Item() {
        super();
    }

    public Integer getIid() {
        return iid;
    }

    public void setIid(Integer iid) {
        this.iid = iid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getIname() {
        return iname;
    }

    public void setIname(String iname) {
        this.iname = iname == null ? null : iname.trim();
    }

    public Integer getIimage() {
        return iimage;
    }

    public void setIimage(Integer iimage) {
        this.iimage = iimage;
    }

    public Integer getItype() {
        return itype;
    }

    public void setItype(Integer itype) {
        this.itype = itype;
    }

    public Date getItime() {
        return itime;
    }

    public String getChangedItime(){
        DateFormat dateFormat=DateFormat.getDateTimeInstance();
        //格式化日期时间　　2019年5月13日 下午10:06:07
        return dateFormat.format(itime);
    }

    public void setItime(Date itime) {
        this.itime = itime;
    }

    public Integer getIbrowse() {
        return ibrowse;
    }

    public void setIbrowse(Integer ibrowse) {
        this.ibrowse = ibrowse;
    }

    public String getIintro() {
        return iintro;
    }

    public void setIintro(String iintro) {
        this.iintro = iintro == null ? null : iintro.trim();
    }
}