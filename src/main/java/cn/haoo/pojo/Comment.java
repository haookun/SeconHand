package cn.haoo.pojo;

import lombok.Data;

import java.text.DateFormat;
import java.util.Date;

@Data
public class Comment {
    private Integer cid;

    private Integer uid;

    private Integer iid;

    private Integer cidp;

    private Date ctime;

    private Integer cread;

    private String ccontent;

    public Comment(Integer cid, Integer uid, Integer iid, Integer cidp, Date ctime, Integer cread, String ccontent) {
        this.cid = cid;
        this.uid = uid;
        this.iid = iid;
        this.cidp = cidp;
        this.ctime = ctime;
        this.cread = cread;
        String s = this.ccontent = ccontent;
    }

    public Comment() {
        super();
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getIid() {
        return iid;
    }

    public void setIid(Integer iid) {
        this.iid = iid;
    }

    public Integer getCidp() {
        return cidp;
    }

    public void setCidp(Integer cidp) {
        this.cidp = cidp;
    }

    public Date getCtime() {
        return ctime;
    }

    public void setCtime(Date ctime) {
        this.ctime = ctime;
    }

    public String getChangedCtime(){
        DateFormat dateFormat=DateFormat.getDateTimeInstance();
        //格式化日期时间　　2019年5月13日 下午10:06:07
        return dateFormat.format(ctime);
    }

    public Integer getCread() {
        return cread;
    }

    public void setCread(Integer cread) {
        this.cread = cread;
    }

    public String getCcontent() {
        return ccontent;
    }

    public void setCcontent(String ccontent) {
        this.ccontent = ccontent == null ? null : ccontent.trim();
    }
}