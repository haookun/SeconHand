package cn.haoo.pojo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import lombok.Data;

/**
 * greatread
 * @author 
 */
@Data
public class Greatread implements Serializable {
    /**
     * 通知编号（主键）自动分配
     */
    private Integer gid;

    /**
     * 点赞用户
     */
    private Integer uid;

    /**
     * 用户名，不允许重复
     */
    private String uname;

    /**
     * 通知时间（自动分配）
     */
    private Date gtime;

    /**
     * 帖编号（主键）自动分配
     */
    private Integer iidp;

    /**
     * 发帖用户编号（外键）
     */
    private Integer iuidp;

    /**
     * 用户名，不允许重复
     */
    private String iunamep;

    /**
     * 帖标题，长度限制在20字以内
     */
    private String inamep;

    /**
     * 评论id（主键）自动分配
     */
    private Integer cidp;

    /**
     * 用户id（外键）
     */
    private Integer cuidp;

    /**
     * 评论内容
     */
    private String ccontentp;

    /**
     * 用户名，不允许重复
     */
    private String cunamep;

    /**
     * 点赞时间被读状态（已读1，未读0（默认））
     */
    private Integer gread;

    private static final long serialVersionUID = 1L;

    public Integer getGid() {
        return gid;
    }

    public void setGid(Integer gid) {
        this.gid = gid;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public Date getGtime() {
        return gtime;
    }

    public void setGtime(Date gtime) {
        this.gtime = gtime;
    }

    public Integer getIidp() {
        return iidp;
    }

    public void setIidp(Integer iidp) {
        this.iidp = iidp;
    }

    public Integer getIuidp() {
        return iuidp;
    }

    public void setIuidp(Integer iuidp) {
        this.iuidp = iuidp;
    }

    public String getIunamep() {
        return iunamep;
    }

    public void setIunamep(String iunamep) {
        this.iunamep = iunamep;
    }

    public String getInamep() {
        return inamep;
    }

    public void setInamep(String inamep) {
        this.inamep = inamep;
    }

    public Integer getCidp() {
        return cidp;
    }

    public void setCidp(Integer cidp) {
        this.cidp = cidp;
    }

    public Integer getCuidp() {
        return cuidp;
    }

    public void setCuidp(Integer cuidp) {
        this.cuidp = cuidp;
    }

    public String getCcontentp() {
        return ccontentp;
    }

    public void setCcontentp(String ccontentp) {
        this.ccontentp = ccontentp;
    }

    public String getCunamep() {
        return cunamep;
    }

    public void setCunamep(String cunamep) {
        this.cunamep = cunamep;
    }

    public Integer getGread() {
        return gread;
    }

    public void setGread(Integer gread) {
        this.gread = gread;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String timeToNow(Date oldtime) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss");
        Date today = new Date(System.currentTimeMillis());
        /*时间差*/
        long to = today.getTime();
        long from = oldtime.getTime();
        String mark[] = {"分钟", "小时", "天"};
        int time = (int) ((to - from) / (1000 * 60));
        int i = 0;
        for (i = 0; i < 3; i++) {
            if (i == 0 && time >= 60)
                time = time / 60;
            else if (i == 1 && time >= 24)
                time = time / 24;
            else break;
        }
        String timetonow = (time + mark[i]);
        return timetonow;
    }
}