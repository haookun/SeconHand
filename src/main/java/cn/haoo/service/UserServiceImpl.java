package cn.haoo.service;

import cn.haoo.dao.UserMapper;
import cn.haoo.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    //service调用dao层：组合dao
    @Autowired
    private UserMapper userMapper;

    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }

    public int deleteByPrimaryKey(Integer uid) {
        return userMapper.deleteByPrimaryKey(uid);
    }

    public int insert(User record) {
        return userMapper.insert(record);
    }

    public int insertSelective(User record) {
        return userMapper.insertSelective(record);
    }

    public User selectByPrimaryKey(Integer uid) {
        return userMapper.selectByPrimaryKey(uid);
    }

    public User selectByUno(String uno) {
        return userMapper.selectByUno(uno);
    }

    public User selectByUname(String uname) {
        return userMapper.selectByUname(uname);
    }

    @Override
    public List<User> selectByHot() {
        return userMapper.selectByHot();
    }

    public int updateByPrimaryKeySelective(User record) {
        return userMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKeyWithBLOBs(User record) {
        return updateByPrimaryKeyWithBLOBs(record);
    }

    public int updateByPrimaryKey(User record) {
        return userMapper.updateByPrimaryKey(record);
    }

    public List<User> selectAll() {
        return userMapper.selectAll();
    }
}
