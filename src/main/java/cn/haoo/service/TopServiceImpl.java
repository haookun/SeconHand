package cn.haoo.service;

import cn.haoo.dao.TopMapper;
import cn.haoo.pojo.Top;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TopServiceImpl implements TopService{

    @Autowired
    private TopMapper topMapper;

    public int deleteByPrimaryKey(Integer tid) {
        return topMapper.deleteByPrimaryKey(tid);
    }

    @Override
    public int deleteByIid(Integer iid) {
        return topMapper.deleteByIid(iid);
    }

    public int insert(Top record) {
        return topMapper.insert(record);
    }

    public int insertSelective(Top record) {
        return topMapper.insertSelective(record);
    }

    public Top selectByPrimaryKey(Integer tid) {
        return topMapper.selectByPrimaryKey(tid);
    }

    @Override
    public Top selectByIid(Integer iid) {
        return topMapper.selectByIid(iid);
    }

    public List<Top> selectAll() {
        return topMapper.selectAll();
    }

    public int updateByPrimaryKeySelective(Top record) {
        return topMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(Top record) {
        return topMapper.updateByPrimaryKey(record);
    }
}
