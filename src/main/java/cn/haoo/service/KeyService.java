package cn.haoo.service;

import cn.haoo.dao.KeyMapper;
import cn.haoo.pojo.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


public interface KeyService {

    int deleteByPrimaryKey(Integer kid);

    int insert(Key record);

    int insertSelective(Key record);

    Key selectByPrimaryKey(Integer kid);

    List<Key> selectByUid(Integer uid);

    int updateByPrimaryKeySelective(Key record);

    int updateByPrimaryKey(Key record);
}