package cn.haoo.service;

import cn.haoo.dao.GreatMapper;
import cn.haoo.dao.GreatreadMapper;
import cn.haoo.pojo.Greatread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GreatreadServiceImpl implements GreatreadService{
    @Autowired
    GreatreadMapper greatreadMapper;

    public int insert(Greatread record) {
        return 0;
    }

    public int insertSelective(Greatread record) {
        return 0;
    }

    public List<Greatread> selectByUipd(int uidp) {
        return greatreadMapper.selectByUipd(uidp);
    }
}
