package cn.haoo.service;

import cn.haoo.pojo.Top;

import java.util.List;

public interface TopService {
    int deleteByPrimaryKey(Integer tid);

    int deleteByIid(Integer iid);

    int insert(Top record);

    int insertSelective(Top record);

    Top selectByPrimaryKey(Integer tid);

    Top selectByIid(Integer iid);

    List<Top> selectAll();

    int updateByPrimaryKeySelective(Top record);

    int updateByPrimaryKey(Top record);
}