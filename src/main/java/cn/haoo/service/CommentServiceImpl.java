package cn.haoo.service;

import cn.haoo.dao.CommentMapper;
import cn.haoo.pojo.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService{

    @Autowired
    private CommentMapper commentMapper;

    public int deleteByPrimaryKey(Integer cid) {
        return 0;
    }

    public int insert(Comment record) {
        return 0;
    }

    public int insertSelective(Comment record) {
        return commentMapper.insertSelective(record);
    }

    public Comment selectByPrimaryKey(Integer cid) {
        return commentMapper.selectByPrimaryKey(cid);
    }

    public int selectByIid(Integer iid) {
        return commentMapper.selectByIid(iid);
    }

    public List<Comment> selectAllByIid(Integer iid){
        return commentMapper.selectAllByIid(iid);
    }

    @Override
    public List<Comment> selectByUid(Integer uid) {
        return commentMapper.selectByUid(uid);
    }

    public int updateByPrimaryKeySelective(Comment record) {
        return commentMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKeyWithBLOBs(Comment record) {
        return commentMapper.updateByPrimaryKeyWithBLOBs(record);
    }

    public int updateByPrimaryKey(Comment record) {
        return commentMapper.updateByPrimaryKey(record);
    }
}
