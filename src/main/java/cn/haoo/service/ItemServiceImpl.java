package cn.haoo.service;

import cn.haoo.dao.ItemMapper;
import cn.haoo.pojo.IndexView;
import cn.haoo.pojo.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemMapper itemMapper;

    public int deleteByPrimaryKey(Integer iid) {
        return itemMapper.deleteByPrimaryKey(iid);
    }

    public int insert(Item record) {
        return itemMapper.insert(record);
    }

    public int insertSelective(Item record) {
        return itemMapper.insertSelective(record);
    }

    public Item selectByPrimaryKey(Integer iid) {
        return itemMapper.selectByPrimaryKey(iid);
    }

    public List<Item> selectAll() {
        return itemMapper.selectAll();
    }

    public List<Item> selectTop() {
        return itemMapper.selectTop();
    }

    public List<Item> selectByUid(Integer uid) {
        return itemMapper.selectByUid(uid);
    }

    public List<Item> selectByIname(String iname) {
        return itemMapper.selectByIname(iname);
    }

    public List<Item> selectByItype(int itype) {
        return itemMapper.selectByItype(itype);
    }

    public List<Item> selectSeconHand() {
        return itemMapper.selectSeconHand();
    }

    @Override
    public List<Item> selectByHot() {
        return itemMapper.selectByHot();
    }

    public int updateByPrimaryKeySelective(Item record) {
        return itemMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKeyWithBLOBs(Item record) {
        return itemMapper.updateByPrimaryKeyWithBLOBs(record);
    }

    public int updateByPrimaryKey(Item record) {
        return itemMapper.updateByPrimaryKey(record);
    }

}
