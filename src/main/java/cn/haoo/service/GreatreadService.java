package cn.haoo.service;

import cn.haoo.pojo.Greatread;

import java.util.List;

public interface GreatreadService {
    int insert(Greatread record);

    int insertSelective(Greatread record);

    List<Greatread> selectByUipd(int uidp);
}