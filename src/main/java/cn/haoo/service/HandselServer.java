package cn.haoo.service;

import cn.haoo.pojo.Handsel;
import javafx.scene.chart.PieChart;

import java.util.Date;
import java.util.List;

public interface HandselServer {
    int deleteByPrimaryKey(Integer hid);

    int insert(Handsel record);

    int insertSelective(Handsel record);

    Handsel selectByPrimaryKey(Integer hid);

    Handsel selectByIid(Integer iid);

    List<Handsel> selectByhtime(Date htime);

    int updateByPrimaryKeySelective(Handsel record);

    int updateByPrimaryKey(Handsel record);
}