package cn.haoo.service;

import cn.haoo.pojo.Great;
import org.springframework.stereotype.Service;


public interface GreatService {
    int deleteByPrimaryKey(Integer gid);

    int insert(Great record);

    int insertSelective(Great record);

    Great selectByPrimaryKey(Integer gid);

    int selectByIid(Integer iid);

    int selectByCid(Integer cid);

    int updateByPrimaryKeySelective(Great record);

    int updateByPrimaryKey(Great record);
}