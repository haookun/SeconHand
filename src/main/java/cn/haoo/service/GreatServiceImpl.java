package cn.haoo.service;

import cn.haoo.dao.GreatMapper;
import cn.haoo.pojo.Great;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GreatServiceImpl implements GreatService {

    @Autowired
    private GreatMapper greatMapper;

    public int deleteByPrimaryKey(Integer gid) {
        return 0;
    }

    public int insert(Great record) {
        return 0;
    }

    public int insertSelective(Great record) {
        return greatMapper.insertSelective(record);
    }

    public Great selectByPrimaryKey(Integer gid) {
        return greatMapper.selectByPrimaryKey(gid);
    }

    public int selectByIid(Integer iid) {
        return greatMapper.selectByIid(iid);
    }

    public int selectByCid(Integer cid) {
        return greatMapper.selectByCid(cid);
    }

    public int updateByPrimaryKeySelective(Great record) {
        return greatMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(Great record) {
        return greatMapper.updateByPrimaryKey(record);
    }

}
