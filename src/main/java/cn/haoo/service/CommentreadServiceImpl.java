package cn.haoo.service;

import cn.haoo.dao.CommentreadMapper;
import cn.haoo.pojo.Comment;
import cn.haoo.pojo.Commentread;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentreadServiceImpl implements CommentreadService {
    @Autowired
    CommentreadMapper commentreadMapper;

    public int insert(Commentread record) {
        return 0;
    }

    public int insertSelective(Commentread record) {
        return 0;
    }

    public List<Commentread> selectByUidp(int uidp) {
        return commentreadMapper.selectByUidp(uidp);
    }

    public List<Commentread> selectByUid(int uid) {
        return commentreadMapper.selectByUid(uid);
    }

    public List<Commentread> selectALl() {
        return commentreadMapper.selectALl();
    }
}
