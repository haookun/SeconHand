package cn.haoo.service;

import cn.haoo.dao.GreatMapper;
import cn.haoo.dao.HandselMapper;
import cn.haoo.pojo.Handsel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class HandselServerImpl implements HandselServer{

    @Autowired
    private HandselMapper handselMapper;

    public int deleteByPrimaryKey(Integer hid) {
        return handselMapper.deleteByPrimaryKey(hid);
    }

    public int insert(Handsel record) {
        return handselMapper.insert(record);
    }

    public int insertSelective(Handsel record) {
        return handselMapper.insertSelective(record);
    }

    public Handsel selectByPrimaryKey(Integer hid) {
        return handselMapper.selectByPrimaryKey(hid);
    }

    public Handsel selectByIid(Integer iid) {
        return handselMapper.selectByIid(iid);
    }

    public List<Handsel> selectByhtime(Date htime) {
        return handselMapper.selectByhtime(htime);
    }

    public int updateByPrimaryKeySelective(Handsel record) {
        return handselMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(Handsel record) {
        return handselMapper.updateByPrimaryKey(record);
    }
}
