package cn.haoo.service;

import cn.haoo.pojo.IndexView;
import cn.haoo.pojo.Item;

import java.util.List;

public interface ItemService {
    int deleteByPrimaryKey(Integer iid);

    int insert(Item record);

    int insertSelective(Item record);

    Item selectByPrimaryKey(Integer iid);

    List<Item> selectAll();

    List<Item> selectTop();

    List<Item> selectByUid(Integer uid);

    List<Item> selectByIname(String iname);

    List<Item> selectByItype(int itype);

    List<Item> selectSeconHand();

    List<Item> selectByHot();

    int updateByPrimaryKeySelective(Item record);

    int updateByPrimaryKeyWithBLOBs(Item record);

    int updateByPrimaryKey(Item record);
}