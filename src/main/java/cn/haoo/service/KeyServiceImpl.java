package cn.haoo.service;

import cn.haoo.dao.KeyMapper;
import cn.haoo.pojo.Key;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KeyServiceImpl  implements KeyService{
    @Autowired
    private KeyMapper keyMapper;

    public int deleteByPrimaryKey(Integer kid) {
        return keyMapper.deleteByPrimaryKey(kid);
    }

    public int insert(Key record) {
        return keyMapper.insert(record);
    }

    public int insertSelective(Key record) {
        return keyMapper.insertSelective(record);
    }

    public Key selectByPrimaryKey(Integer kid) {
        return keyMapper.selectByPrimaryKey(kid);
    }

    public List<Key> selectByUid(Integer uid){
        return keyMapper.selectByUid(uid);
    }

    public int updateByPrimaryKeySelective(Key record) {
        return keyMapper.updateByPrimaryKeySelective(record);
    }

    public int updateByPrimaryKey(Key record) {
        return keyMapper.updateByPrimaryKey(record);
    }
}
