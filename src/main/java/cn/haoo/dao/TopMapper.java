package cn.haoo.dao;

import cn.haoo.pojo.Top;

import java.util.List;

public interface TopMapper {
    int deleteByPrimaryKey(Integer tid);

    int deleteByIid(Integer iid);

    int insert(Top record);

    int insertSelective(Top record);

    Top selectByPrimaryKey(Integer tid);

    List<Top> selectAll();

    Top selectByIid(Integer iid);

    int updateByPrimaryKeySelective(Top record);

    int updateByPrimaryKey(Top record);
}