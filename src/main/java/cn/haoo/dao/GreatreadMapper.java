package cn.haoo.dao;

import cn.haoo.pojo.Greatread;

import java.util.List;

public interface GreatreadMapper {
    int insert(Greatread record);

    int insertSelective(Greatread record);

    List<Greatread> selectByUipd(int uidp);
}