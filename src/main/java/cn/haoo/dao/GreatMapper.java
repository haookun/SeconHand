package cn.haoo.dao;

import cn.haoo.pojo.Great;

public interface GreatMapper {
    int deleteByPrimaryKey(Integer gid);

    int insert(Great record);

    int insertSelective(Great record);

    Great selectByPrimaryKey(Integer gid);

    int selectByIid(Integer iid);

    int selectByCid(Integer cid);

    int updateByPrimaryKeySelective(Great record);

    int updateByPrimaryKey(Great record);
}