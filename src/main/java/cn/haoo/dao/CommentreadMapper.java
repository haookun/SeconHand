package cn.haoo.dao;

import cn.haoo.pojo.Comment;
import cn.haoo.pojo.Commentread;

import java.util.List;

public interface CommentreadMapper {
    int insert(Commentread record);

    int insertSelective(Commentread record);

    List<Commentread> selectByUidp(int uidp);

    List<Commentread> selectByUid(int uid);

    List<Commentread> selectALl();
}