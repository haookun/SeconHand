package cn.haoo.dao;

import cn.haoo.pojo.Key;

import java.util.List;

public interface KeyMapper {
    int deleteByPrimaryKey(Integer kid);

    int insert(Key record);

    int insertSelective(Key record);

    Key selectByPrimaryKey(Integer kid);

    List<Key> selectByUid(Integer uid);

    int updateByPrimaryKeySelective(Key record);

    int updateByPrimaryKey(Key record);
}