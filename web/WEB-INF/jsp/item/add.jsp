<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>欢迎来到加帕里集市</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="keywords" content="校园，二手交易，加帕里，论坛">
    <meta name="description" content="校园二手物品交流平台——加帕里集市">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/global.css">
</head>
<body>

<div class="fly-header layui-bg-black">
    <div class="layui-container">
        <a class="fly-logo" href="${pageContext.request.contextPath}/index">
            <img src="${pageContext.request.contextPath}/static/images/logo.png" alt="layui">
        </a>

        <ul class="layui-nav fly-nav-user">
            <c:choose>
                <c:when test="${empty user}">
                    <!-- 未登入的状态 -->
                    <li class="layui-nav-item">
                        <a class="iconfont icon-touxiang layui-hide-xs"
                           href="${pageContext.request.contextPath}/user/login"></a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="${pageContext.request.contextPath}/user/login">登入</a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="${pageContext.request.contextPath}/user/regist">注册</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <!-- 登入后的状态 -->
                    <li class="layui-nav-item">
                        <a class="fly-nav-avatar" href="javascript:;">
                            <cite class="layui-hide-xs">${user.getUname()}</cite>
                            <c:if test="${user.getUauth() == 1}"><i
                                    class="layui-badge fly-badge-vip layui-hide-xs">管理员</i></c:if>
                            <img src="${pageContext.request.contextPath}/user/getpic?uid=${user.getUid()}">
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a href="${pageContext.request.contextPath}/user/set"><i class="layui-icon">&#xe620;</i>基本设置</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/message"><i
                                    class="iconfont icon-tongzhi" style="top: 4px;"></i>消息</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/key"><i class="layui-icon">&#xe66e;</i>我的关注</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/home"><i class="layui-icon"
                                                                                          style="margin-left: 2px; font-size: 22px;">&#xe68e;</i>我的主页</a>
                            </dd>
                            <hr style="margin: 5px 0;">
                            <dd><a href="${pageContext.request.contextPath}/user/logout"
                                   style="text-align: center;">退出</a></dd>
                        </dl>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</div>

<div class="layui-container fly-marginTop">
    <div class="fly-panel" pad20 style="padding-top: 5px;">
        <!--<div class="fly-none">没有权限</div>-->
        <div class="layui-form layui-form-pane">
            <div class="layui-tab layui-tab-brief" lay-filter="user">
                <ul class="layui-tab-title">
                    <li class="layui-this">发表新帖</li><!-- 编辑帖子 -->
                </ul>
                <div class="layui-form layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
                    <div class="layui-tab-item layui-show">
                        <c:choose>
                            <c:when test="${not empty item}"><form action="${pageContext.request.contextPath}/item/update" method="post"></c:when>
                            <c:otherwise><form action="${pageContext.request.contextPath}/item/add" method="post"></c:otherwise>
                        </c:choose>

                            <div class="layui-row layui-col-space15 layui-form-item">
                                <input type="hidden" id="L_iid" name="iid" value="${iid}">
                                <input type="hidden" id="L_uid" name="uid" value="${user.getUid()}">
                                <div class="layui-col-md3">
                                    <label class="layui-form-label">分类</label>
                                    <div class="layui-input-block">
                                        <select lay-verify="required" name="itype" lay-filter="column">
                                            <option></option>
                                            <c:if test="${user.getUauth() == 1}"><i
                                                    class="layui-badge fly-badge-vip layui-hide-xs">
                                                <option value="1" <c:if test="${item.getItype()==1}">selected</c:if>>
                                                    公告
                                                </option>
                                            </i></c:if>
                                            <option value="2" <c:if test="${item.getItype()==2}">selected</c:if>>图文
                                            </option>
                                            <option value="3" <c:if test="${item.getItype()==3}">selected</c:if>>二手
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="layui-col-md9">
                                    <label for="L_title" class="layui-form-label">标题</label>
                                    <div class="layui-input-block">
                                        <input type="text" id="L_title" name="iname" required lay-verify="required"
                                               autocomplete="off" class="layui-input" value="${item.getIname()}">
                                        <!-- <input type="hidden" name="id" value="{{d.edit.id}}"> -->
                                    </div>
                                </div>
                            </div>
                            <%--
                            <div class="layui-row layui-col-space15 layui-form-item layui-hide" id="LAY_quiz">
                              <div class="layui-col-md3">
                                <label class="layui-form-label">所属产品</label>
                                <div class="layui-input-block">
                                  <select name="project">
                                    <option></option>
                                    <option value="layui">layui</option>
                                    <option value="独立版layer">独立版layer</option>
                                    <option value="独立版layDate">独立版layDate</option>
                                    <option value="LayIM">LayIM</option>
                                    <option value="Fly社区模板">Fly社区模板</option>
                                  </select>
                                </div>
                              </div>
                              <div class="layui-col-md3">
                                <label class="layui-form-label" for="L_version">版本号</label>
                                <div class="layui-input-block">
                                  <input type="text" id="L_version" value="" name="version" autocomplete="off" class="layui-input">
                                </div>
                              </div>
                              <div class="layui-col-md6">
                                <label class="layui-form-label" for="L_browser">浏览器</label>
                                <div class="layui-input-block">
                                  <input type="text" id="L_browser"  value="" name="browser" placeholder="浏览器名称及版本，如：IE 11" autocomplete="off" class="layui-input">
                                </div>
                              </div>
                            </div>
                            --%>
                            <div class="layui-form-item layui-form-text">
                                <div class="layui-input-block">
                                    <textarea id="L_content" name="iintro" required lay-verify="required"
                                              placeholder="详细描述" class="layui-textarea fly-editor"
                                              style="height: 260px;">${item.getIintro()}</textarea>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <div class="layui-inline">
                                    <label class="layui-form-label">捐赠日期</label>
                                    <div class="layui-input-inline" style="width: 190px;">
                                        <input type="text" class="layui-input" id="L_handsel" name="htime"
                                               laceholder="yyyy-MM-dd" value="${htime.getHtime()}">
                                    </div>
                                    <div class="layui-form-mid layui-word-aux">注意*在到达捐赠日期后，物品将会显示在“首页-捐赠”分栏中，送给有需要的人
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_vercode" class="layui-form-label">人类验证</label>
                                <div class="layui-input-inline">
                                    <input type="" id="L_vercode" name="vercode" required lay-verify="required"
                                           placeholder="请回答后面的问题" autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid">
                                    <span style="color: #c00;">1+1=?</span>
                                </div>
                            </div>
                            <div class="layui-form-item">

                                <button type="submit" class="layui-btn" lay-filter="*" lay-submit>立即发布</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="fly-footer">
    <p><a href="#" target="_blank">加帕里集市</a> 2020 &copy; <a href="#" target="_blank">Haoo</a></p>
</div>

<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    layui.cache.page = '';
    layui.cache.user = {
        username: '${user.getUname()}'
        , uid: '${user.getUno()}'
        , avatar: '${pageContext.request.contextPath}/user/getpic?uid=${user.getUid()}'
        , sex: '${user.getUsex()}'
    };
    layui.config({
        version: "3.0.0"
        , base: '${pageContext.request.contextPath}/static/mods/' //这里实际使用时，建议改成绝对路径
    }).extend({
        fly: 'index'
    }).use('fly');
</script>
<script>
    layui.use('laydate', function () {
        var laydate = layui.laydate;
        //执行一个laydate实例
        laydate.render({
            elem: '#L_handsel' //指定元素
        });
    });
</script>

</body>
</html>