<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>欢迎来到加帕里集市</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="keywords" content="校园，二手交易，加帕里，论坛">
    <meta name="description" content="校园二手物品交流平台——加帕里集市">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/global.css">
</head>
<body>

<div class="fly-header layui-bg-black">
    <div class="layui-container">
        <a class="fly-logo" href="${pageContext.request.contextPath}/index">
            <img src="${pageContext.request.contextPath}/static/images/logo.png" alt="layui">
        </a>

        <ul class="layui-nav fly-nav-user">
            <c:choose>
                <c:when test="${empty user}">
                    <!-- 未登入的状态 -->
                    <li class="layui-nav-item">
                        <a class="iconfont icon-touxiang layui-hide-xs"
                           href="${pageContext.request.contextPath}/user/login"></a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="${pageContext.request.contextPath}/user/login">登入</a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="${pageContext.request.contextPath}/user/regist">注册</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <!-- 登入后的状态 -->
                    <li class="layui-nav-item">
                        <a class="fly-nav-avatar" href="javascript:;">
                            <cite class="layui-hide-xs">${user.getUname()}</cite>
                            <c:if test="${user.getUauth() == 1}"><i
                                    class="layui-badge fly-badge-vip layui-hide-xs">管理员</i></c:if>
                            <img src="${pageContext.request.contextPath}/user/getpic?uid=${user.getUid()}">
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a href="${pageContext.request.contextPath}/user/set"><i class="layui-icon">&#xe620;</i>基本设置</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/message"><i
                                    class="iconfont icon-tongzhi" style="top: 4px;"></i>消息</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/key"><i class="layui-icon">&#xe66e;</i>我的关注</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/home"><i class="layui-icon"
                                                                                          style="margin-left: 2px; font-size: 22px;">&#xe68e;</i>我的主页</a>
                            </dd>
                            <hr style="margin: 5px 0;">
                            <dd><a href="${pageContext.request.contextPath}/user/logout"
                                   style="text-align: center;">退出</a></dd>
                        </dl>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</div>

<div class="fly-panel fly-column">
    <div class="layui-container">
        <%--首页（显示所有消息）、
        公告（显示所有公告）、
        二手（按照时间，显示所有二手帖）、
        图文（按照时间，显示所有图文帖）、
        我的关注（显示包含关键词的所有内容，添加删除关键词）
        小红点<span class="layui-badge-dot"></span>--%>
        <ul class="layui-clear">
            <li class="<c:if test="${empty itype}">layui-hide-xs layui-this</c:if>"><a href="${pageContext.request.contextPath}/index">首页</a></li>
            <li class="<c:if test="${itype==7}">layui-hide-xs layui-this</c:if>"><a href="${pageContext.request.contextPath}/index/type?type=热议">热议</a></li>
            <li class="<c:if test="${itype==1}">layui-hide-xs layui-this</c:if>"><a href="${pageContext.request.contextPath}/index/type?type=公告">公告</a></li>
            <li class="<c:if test="${itype==2}">layui-hide-xs layui-this</c:if>"><a href="${pageContext.request.contextPath}/index/type?type=图文">图文</a></li>
            <li class="<c:if test="${itype==3||itype==4||itype==5}">layui-hide-xs layui-this</c:if>"><a href="${pageContext.request.contextPath}/index/type?type=二手">二手</a></li>
            <li class="<c:if test="${itype==6}">layui-hide-xs layui-this</c:if>"><a href="${pageContext.request.contextPath}/index/type?type=捐赠">捐赠</a></li>
            <c:if test="${not empty user}">
                <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block">
                    <span class="fly-mid"></span>
                </li>
                <!-- 用户登入后显示 -->
                <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block"><a
                        href="${pageContext.request.contextPath}/user/home">我的发布</a></li>
                <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block"><a
                        href="${pageContext.request.contextPath}/focus">我的关注</a></li>
            </c:if>
        </ul>

        <div class="fly-column-right layui-hide-xs">
            <span class="fly-search"><i class="layui-icon"></i></span>
            <a href="${pageContext.request.contextPath}/item/add" class="layui-btn">发布</a>
        </div>
        <div class="layui-hide-sm layui-show-xs-block"
             style="margin-top: -10px; padding-bottom: 10px; text-align: center;">
            <a href="${pageContext.request.contextPath}/item/add" class="layui-btn">发布</a>
        </div>
    </div>
</div>

<div class="layui-container">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md8 content detail">
            <div class="fly-panel detail-box">
                <h1>${itemview.getIname()}</h1>
                <div class="fly-detail-info">
                    <c:choose>
                        <c:when test="${itemview.getItype() == 1}"><a class="layui-badge layui-bg-black">
                            公告 </a></c:when>
                        <c:when test="${itemview.getItype() == 2}"><a
                                class="layui-badge layui-bg-green fly-detail-column"> 图文 </a></c:when>
                        <c:otherwise><a class="layui-badge layui-bg-green fly-detail-column"> 二手 </a></c:otherwise>
                    </c:choose>
                    <c:if test="${itemview.getItype() == 4}"><a class="layui-badge"
                                                                style="background-color: #999;">已结束</a></c:if>


                    <span class="fly-list-nums">
                        <a href="${pageContext.request.contextPath}/item/great?iid=${itemview.getIid()}&cid=-1"><i
                                class="iconfont icon-zan"></i> ${itemview.getGreat()}</a>
                        <a href="#comment"><i class="iconfont" title="评论">&#xe60c;</i> ${itemview.getComment()}</a>
                        <i class="iconfont" title="浏览量">&#xe60b;</i> ${itemview.getIbrowse()}
                    </span>
                </div>
                <div class="detail-about">
                    <a class="fly-avatar"
                       href="${pageContext.request.contextPath}/user/neighbor?uid=${itemview.getUid()}">
                        <img src="${pageContext.request.contextPath}/user/getpic?uid=${itemview.getUid()}"
                             alt="${itemview.getUname()}">
                    </a>
                    <div class="fly-detail-user">
                        <a href="${pageContext.request.contextPath}/user/neighbor?uid=${itemview.getUid()}"
                           class="fly-link">
                            <cite>${itemview.getUname()}</cite>
                            <c:if test="${ti.getUauth() == 1}"><i class="layui-badge fly-badge-vip"
                                                                  title="管理员">管理员</i></c:if>
                        </a>
                        <span>${item.getChangedItime()}</span>
                        <%--格式化日期时间　　2019年5月13日 下午10:06:07--%>
                    </div>

                    <div class="detail-hits" id="LAY_jieAdmin" data-id="123">
                        <c:if test="${itemview.getUid() == user.getUid() || user.getUauth() == 1}">
                            <span class="layui-btn layui-btn-xs jie-admin" type="edit"><a
                                    href="${pageContext.request.contextPath}/item/update?iid=${itemview.getIid()}">编辑此贴</a></span>

                            <a href="${pageContext.request.contextPath}/item/delete?iid=${itemview.getIid()}"><span
                                    class="layui-btn layui-btn-xs jie-admin" type="del">删除</span></a>
                            <c:if test="${itemview.getItype() == 3}">
                                <a href="${pageContext.request.contextPath}/item/over?iid=${itemview.getIid()}"><span
                                        class="layui-btn layui-btn-xs jie-admin" type="set" field="stick"
                                        rank="1">结束此贴</span></a>
                            </c:if>

                            <%--<c:if test="${user.getUauth() == 1}">
                                <span class="layui-btn layui-btn-xs jie-admin" type="set" field="stick"
                                      rank="1">置顶</span>
                                <!-- <span class="layui-btn layui-btn-xs jie-admin" type="set" field="stick" rank="0" style="background-color:#ccc;">取消置顶</span> -->

                                <span class="layui-btn layui-btn-xs jie-admin" type="set" field="status"
                                      rank="1">加精</span>
                                <!-- <span class="layui-btn layui-btn-xs jie-admin" type="set" field="status" rank="0" style="background-color:#ccc;">取消加精</span> -->
                            </c:if>--%>
                        </c:if>
                    </div>
                    <%--<div class="fly-admin-box" data-id="123">
                    </div>--%>
                </div>
                <div class="detail-body photos">
                    介绍
                    <hr>
                    <p>${item.iintro}</p>

                    图片
                    <hr>
                    <%--<c:forEach var="i" begin="1" end="${item.iimage}">--%>
                    <p>
                        <img src="${pageContext.request.contextPath}/item/getimg?iid=${item.iid}"
                             alt="${item.iname}图片">
                    </p>
                    <%--</c:forEach>--%>
                </div>
            </div>

            <div class="fly-panel detail-box" id="flyReply">
                <fieldset class="layui-elem-field layui-field-title" style="text-align: center;">
                    <legend>回帖</legend>
                </fieldset>

                <ul class="jieda" id="jieda">

                    <c:forEach var="i" items="${requestScope.get('clist')}">
                        <li data-id="111" class="jieda-daan">
                            <a name="item-1111111111"></a>
                            <div class="detail-about detail-about-reply">
                                <a class="fly-avatar"
                                   href="${pageContext.request.contextPath}/user/neighbor?uid=${i.user.getUid()}">
                                    <img src="${pageContext.request.contextPath}/user/getpic?uid=${i.user.getUid()}"
                                         alt="${i.user.getUname()}">
                                </a>
                                <div class="fly-detail-user">
                                    <a href="${pageContext.request.contextPath}/user/neighbor?uid=${i.user.getUid()}"
                                       class="fly-link">
                                        <cite>${i.user.getUname()}</cite>
                                        <c:if test="${i.user.getUauth() == 1}"><i class="layui-badge fly-badge-vip"
                                                                                  title="管理员">管理员</i></c:if>
                                    </a>

                                    <span><c:if test="${i.user.getUid() == itemview.getUid()}">(楼主)</c:if>
                                        <c:if test="${i.user.getUid() == user.getUid()}">(我的)</c:if></span>
                                </div>

                                <div class="detail-hits">
                                    <span>${i.comment.getChangedCtime()}</span>
                                </div>

                                    <%--<i class="iconfont icon-caina" title="最佳答案"></i>--%>
                            </div>

                            <c:if test="${not empty i.commentp}">
                                <pre>回复：@<a
                                        href="${pageContext.request.contextPath}/user/neighbor?uid=${i.userp.getUid()}"
                                        class="fly-link"><cite>${i.userp.getUname()}</cite></a>
                                    <div class="detail-hits"><span>${i.commentp.getCcontent()}</span></div></pre>
                            </c:if>
                            <div class="detail-body jieda-body photos">
                                <p>${i.comment.getCcontent()}</p>
                            </div>
                            <div class="jieda-reply">
                              <span class="jieda-zan zanok" type="zan">
                                  <a href="${pageContext.request.contextPath}/item/great?iid=${i.comment.getIid()}&cid=${i.comment.getCid()}"><i
                                          class="iconfont icon-zan"></i><em>${i.getGreat()} </em></a>
                              </span>
                                <span type="reply">
                                    <a href="#comment" onclick="reply(${i.comment.getCid()})"><i
                                            class="iconfont icon-svgmoban53"></i>回复</a>
                              </span>
                                <c:if test="${user.getUid()==i.comment.getUid()}">
                                    <div class="jieda-admin">
                                        <span type="edit">编辑</span>
                                        <span type="del">删除</span>
                                        <!-- <span class="jieda-accept" type="accept">采纳</span> -->
                                    </div>
                                </c:if>
                            </div>
                        </li>
                    </c:forEach>

                    <c:if test="${empty clist}">
                        <!-- 无数据时 -->
                        <li class="fly-none">消灭零回复!</li>
                    </c:if>

                </ul>

                <div class="layui-form layui-form-pane">
                    <form action="${pageContext.request.contextPath}/comm/reply" method="post">
                        <div class="layui-form-item layui-form-text">
                            <a name="comment"></a>
                            <div class="layui-input-block">
                                <textarea id="L_content" name="ccontent" required lay-verify="required"
                                          placeholder="谈谈你的见解吧" class="layui-textarea fly-editor"
                                          style="height: 150px;"></textarea>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <input type="hidden" name="iid" value="${itemview.getIid()}">
                            <input type="hidden" name="uid" value="${user.getUid()}">
                            <input type="hidden" id="cidp" name="cidp" value="">
                            <button class="layui-btn" lay-filter="*" lay-submit>提交回复</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="layui-col-md4">
            <dl class="fly-panel fly-list-one">
                <dt class="fly-panel-title">本周热议</dt>
                <%--最多5条--%>
                <% for (int i = 0; i < 3; i++) {
                %>
                <dd>
                    <a href="jie/detail.html">test</a>
                    <span><i class="iconfont icon-pinglun1"></i> 16</span>
                </dd>
                <%
                    }%>
                <%--
                无数据时
                <div class="fly-none">没有相关数据</div>
                 --%>
            </dl>
        </div>
    </div>
</div>

<div class="fly-footer">
    <p><a href="#" target="_blank">加帕里集市</a> 2020 &copy; <a href="#" target="_blank">Haoo</a></p>
</div>

<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script src="http://code.jquery.com/jquery-latest.js"></script>
<script>
    layui.cache.page = '';
    layui.cache.user = {
        username: '${user.getUname()}'
        , uid: '${user.getUno()}'
        , avatar: '${pageContext.request.contextPath}/user/getpic?uid=${user.getUid()}'
        , sex: '${user.getUsex()}'
    };
    layui.config({
        version: "3.0.0"
        , base: '${pageContext.request.contextPath}/static/mods/' //这里实际使用时，建议改成绝对路径
    }).extend({
        fly: 'index'
    }).use(['fly', 'face'], function () {
        var $ = layui.$
            , fly = layui.fly;
        //如果你是采用模版自带的编辑器，你需要开启以下语句来解析。
        /*
        $('.detail-body').each(function(){
          var othis = $(this), html = othis.html();
          othis.html(fly.content(html));
        });
        */
    });
</script>
<script type="text/javascript">
    function reply(cidp) {
        $("#cidp").attr("value", cidp);
    }
</script>

</body>
</html>