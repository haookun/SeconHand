<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>加帕里集市后台</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">加帕里集市后台</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <%--<ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="">控制台</a></li>
            <li class="layui-nav-item"><a href="">商品管理</a></li>
            <li class="layui-nav-item"><a href="">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
        </ul>--%>
        <ul class="layui-nav fly-nav-user layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    ${user.getUname()}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="${pageContext.request.contextPath}/user/set">基本设置</a></dd>
                    <dd><a href="${pageContext.request.contextPath}/user/message">消息</a></dd>
                    <dd><a href="${pageContext.request.contextPath}/user/home">我的主页</a></dd>
                    <dd><a href="${pageContext.request.contextPath}/user/logout">退出</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/index">回到首页</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/admin">序</a></li>
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">帖</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${pageContext.request.contextPath}/admin/item">所有帖</a></dd>
                        <dd><a href="${pageContext.request.contextPath}/admin/item/top">置顶帖</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">用户</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${pageContext.request.contextPath}/admin/user">所有用户</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">留言&评论</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${pageContext.request.contextPath}/admin/comm">所有留言&评论</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/admin">结</a></li>
            </ul>
        </div>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->

        <div class="layui-row layui-col-space10" style="padding: 15px;">
            <div class="layui-col-md9">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 50px;">
                    <legend>所有帖</legend>
                </fieldset>
                <table class="layui-table" lay-even="" lay-skin="nob">
                    <%--<colgroup>
                        <col width="150">
                        <col width="150">
                        <col width="200">
                        <col>
                    </colgroup>--%>
                    <thead>
                    <tr>
                        <th>帖id</th>
                        <%--1--%>
                        <th>帖名</th>
                        <%--2--%>
                        <th>用户id</th>
                        <%--3--%>
                        <th>发帖用户名</th>
                        <%--4--%>
                        <th>帖类型</th>
                        <%--6--%>
                        <th>发帖时间</th>
                        <%--7--%>
                        <th>浏览/赞/评论</th>
                        <%--8--%>
                        <th>操作</th>
                        <%--10--%>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="i" items="${requestScope.get('toplist')}">
                        <tr>

                            <th>${i.getIid()}</th>
                                <%--1--%>
                            <th>
                                <a href="${pageContext.request.contextPath}/item/detail?iid=${i.getIid()}">${i.getIname()}</a>
                            </th>
                                <%--2--%>
                            <th>${i.getIid()}</th>
                                <%--3--%>
                            <th>
                                <a href="${pageContext.request.contextPath}/user/neighbor?uid=${i.getUid()}">${i.getUname()}</a>
                            </th>
                                <%--4--%>
                            <th><c:choose>
                                <c:when test="${i.getItype() == 1}">公告</c:when>
                                <c:when test="${i.getItype() == 2}">图文</c:when>
                                <c:when test="${i.getItype() == 3}">未结束</c:when>
                                <c:when test="${i.getItype() == 4}">结束</c:when>
                            </c:choose></th>
                                <%--6--%>
                            <th>${i.getItime()}</th>
                                <%--7--%>
                            <th>${i.getIbrowse()}/${i.getGreat()}/${i.getComment()}</th>
                                <%--8--%>
                            <th>
                                <button type="button" class="layui-btn layui-btn-danger layui-btn-radius layui-btn-xs"
                                        onclick="location.href='${pageContext.request.contextPath}/admin/item/top/delete?iid=${i.getIid()}'">
                                    删除
                                </button>
                            </th>
                                <%--10--%>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>

    </div>

    <div class="layui-footer">
        <p><a href="#" target="_blank">加帕里集市</a> 2020 &copy; <a href="#" target="_blank">Haoo</a></p>
    </div>

    <script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
    <script>
        //JavaScript代码区域
        layui.use('element', function () {
            var element = layui.element;

        });
    </script>
</div>
</body>
</html>