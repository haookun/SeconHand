<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>欢迎来到加帕里集市</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="keywords" content="校园，二手交易，加帕里，论坛">
    <meta name="description" content="校园二手物品交流平台——加帕里集市">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/global.css">
</head>
<body style="margin-top: 65px;">

<div class="fly-header layui-bg-black">
    <div class="layui-container">
        <a class="fly-logo" href="${pageContext.request.contextPath}/index">
            <img src="${pageContext.request.contextPath}/static/images/logo.png" alt="layui">
        </a>

        <ul class="layui-nav fly-nav-user">
            <c:choose>
                <c:when test="${empty user}">
                    <!-- 未登入的状态 -->
                    <li class="layui-nav-item">
                        <a class="iconfont icon-touxiang layui-hide-xs"
                           href="${pageContext.request.contextPath}/user/login"></a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="${pageContext.request.contextPath}/user/login">登入</a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="${pageContext.request.contextPath}/user/regist">注册</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <!-- 登入后的状态 -->
                    <li class="layui-nav-item">
                        <a class="fly-nav-avatar" href="javascript:;">
                            <cite class="layui-hide-xs">${user.getUname()}</cite>
                            <c:if test="${user.getUauth() == 1}"><i
                                    class="layui-badge fly-badge-vip layui-hide-xs">管理员</i></c:if>
                            <img src="${pageContext.request.contextPath}/user/getpic?uid=${user.getUid()}">
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a href="${pageContext.request.contextPath}/user/set"><i class="layui-icon">&#xe620;</i>基本设置</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/message"><i
                                    class="iconfont icon-tongzhi" style="top: 4px;"></i>消息</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/key"><i class="layui-icon">&#xe66e;</i>我的关注</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/home"><i class="layui-icon"
                                                                                          style="margin-left: 2px; font-size: 22px;">&#xe68e;</i>我的主页</a>
                            </dd>
                            <hr style="margin: 5px 0;">
                            <dd><a href="${pageContext.request.contextPath}/user/logout"
                                   style="text-align: center;">退出</a></dd>
                        </dl>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</div>

<div class="fly-home fly-panel" <%--style="background-image: url();"--%>>
    <img src="${pageContext.request.contextPath}/user/getpic?uid=${owner.getUid()}" alt="${owner.getUname()}">
    <h1>
        ${owner.getUname()}
        <c:choose>
            <c:when test="${owner.getUsex() == '男'}"><i class="iconfont icon-nan"></i></c:when>
            <c:when test="${owner.getUsex() == '女'}"><i class="iconfont icon-nv"></i></c:when>
            <c:otherwise></c:otherwise>
        </c:choose>
        <c:if test="${owner.getUauth() == 1}"><span style="color:#c00;">（管理员）</span></c:if>
        <!--
        <span style="color:#5FB878;">（社区之光）</span>
        <span>（该号已被封）</span>
        -->
    </h1>

    <%--
        <p style="padding: 10px 0; color: #5FB878;">认证信息：layui 作者</p>
    --%>

    <p class="fly-home-info">
        <i class="iconfont icon-kiss" title="飞吻"></i><span style="color: #FF7200;">66666 赞</span>
        <c:if test="${owner.getUtel() != null}">
            <i class="iconfont layui-icon-dialogue"></i><span>联系方式：${user.getUtel()}</span>
        </c:if>

    </p>

    <p class="fly-home-sign">
        (
        <c:choose>
            <c:when test="${owner.getUother() !=null}">${owner.getUother()}</c:when>
            <c:when test="${owner.getUother() == null}">这个咸鱼太懒了什么都没写</c:when>
            <c:otherwise></c:otherwise>
        </c:choose>
        )
    </p>

    <%--
        <div class="fly-sns" data-user="">
            <a href="javascript:;" class="layui-btn layui-btn-primary fly-imActive" data-type="addFriend">加为好友</a>
            <a href="javascript:;" class="layui-btn layui-btn-normal fly-imActive" data-type="chat">发起会话</a>
        </div>
     --%>

</div>

<div class="layui-container">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md6 fly-home-jie">
            <div class="fly-panel">

                <h3 class="fly-panel-title">${owner.getUname()} 发布的</h3>
                <ul class="jie-row">
                    <c:forEach var="i" items="${requestScope.get('list')}">
                        <li>
                            <c:choose>
                                <c:when test="${i.getItype() == 1}"><span class="fly-badge"> 公告 </span></c:when>
                                <c:when test="${i.getItype() == 2}"><span class="fly-badge"> 图文 </span></c:when>
                                <c:otherwise><span class="fly-badge"> 二手 </span></c:otherwise>
                            </c:choose>
                            <a href="${pageContext.request.contextPath}/item/detail?iid=${i.getIid()}"
                               class="jie-title">${i.getIname()}</a>
                            <i>${i.getItime()}</i>
                            <em class="layui-hide-xs">${i.getIbrowse()}阅/${i.getComment()}评/${i.getGreat()}赞</em>
                        </li>
                    </c:forEach>
                    <c:if test="${list==null}">
                        <div class="fly-none" style="min-height: 50px; padding:30px 0; height:auto;"><i
                                style="font-size:14px;">这位精神小伙好像一条帖子都没发</i></div>
                    </c:if>
                </ul>
            </div>
        </div>

        <div class="layui-col-md6 fly-home-da">
            <div class="fly-panel">
                <h3 class="fly-panel-title">${owner.getUname()} 最近的评论</h3>
                <ul class="home-jieda">
                    <c:forEach var="c" items="${requestScope.get('comm')}">
                        <li>
                            <p>
                                <span>${c.timeToNow(c.getCtime())}前</span>
                                在<a href="${pageContext.request.contextPath}/item/detail?iid=${c.getIidp()}"
                                    target="_blank">${c.getIname()}</a>中回复${c.getCunamep()}：
                            </p>
                            <div class="home-dacontent">
                                    ${c.getCcontent()}
                            </div>
                        </li>
                    </c:forEach>
                    <%--                    <li>
                                            <p>
                                                <span>1分钟前</span>
                                                在<a href="" target="_blank">tips能同时渲染多个吗?</a>中回答：
                                            </p>
                                            <div class="home-dacontent">
                                                尝试给layer.photos加上这个属性试试：
                                                <pre>full: true</pre>
                                                文档没有提及
                                            </div>
                                        </li>
                                        <li>
                                            <p>
                                                <span>5分钟前</span>
                                                在<a href="" target="_blank">在Fly社区用的是什么系统啊?</a>中回答：
                                            </p>
                                            <div class="home-dacontent">
                                                Fly社区采用的是NodeJS。分享出来的只是前端模版
                                            </div>
                                        </li>--%>
                    <c:if test="${empty comm}">
                        <div class="fly-none" style="min-height: 50px; padding:30px 0; height:auto;">
                            <span>没有回答任何问题</span></div>
                    </c:if>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="fly-footer">
    <p><a href="#" target="_blank">加帕里集市</a> 2020 &copy; <a href="#" target="_blank">Haoo</a></p>
</div>

<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    layui.cache.page = '';
    layui.cache.user = {
        username: '${user.getUname()}'
        , uid: '${user.getUid()}'
        , avatar: '${pageContext.request.contextPath}/static/images/avatar/00.jpg'
        , experience: 83
        , sex: '${user.getUsex()}'
    };
    layui.config({
        version: "3.0.0",
        base: '${pageContext.request.contextPath}/static/mods/' //这里实际使用时，建议改成绝对路径
    }).extend({
        fly: 'index'
    }).use('fly');
</script>
</body>
</html>