<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>是新的FRIEND呢</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="keywords" content="校园，二手交易，加帕里，论坛">
    <meta name="description" content="校园二手物品交流平台——加帕里集市">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/global.css">
</head>
<body>

<div class="fly-header layui-bg-black">
    <div class="layui-container">
        <a class="fly-logo" href="${pageContext.request.contextPath}/index">
            <img src="${pageContext.request.contextPath}/static/images/logo.png" alt="layui">
        </a>

        <ul class="layui-nav fly-nav-user">
            <!-- 未登入的状态 -->
            <li class="layui-nav-item">
                <a class="iconfont icon-touxiang layui-hide-xs" href="${pageContext.request.contextPath}/user/login""></a>
            </li>
            <li class="layui-nav-item">
                <a href="${pageContext.request.contextPath}/user/login">登入</a>
            </li>
            <li class="layui-nav-item">
                <a href="${pageContext.request.contextPath}/user/regist">注册</a>
            </li>
        </ul>
    </div>
</div>

<div class="layui-container fly-marginTop">
    <div class="fly-panel fly-panel-user" pad20>
        <div class="layui-tab layui-tab-brief" lay-filter="user">
            <ul class="layui-tab-title">
                <li><a href="${pageContext.request.contextPath}/user/login">登入</a></li>
                <li class="layui-this">注册</li>
            </ul>
            <div class="layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
                <div class="layui-tab-item layui-show">
                    <div class="layui-form layui-form-pane">
                        <form method="post" action="${pageContext.request.contextPath}/user/regist">
                            <div class="layui-form-item">
                                <label for="L_no" class="layui-form-label">账户</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_no" name="uno" required lay-verify="required"
                                           autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid" style="color: #FF5722">${nowarn}</div>
                                <div class="layui-form-mid layui-word-aux">将会成为您唯一的登入名</div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_name" class="layui-form-label">昵称</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_name" name="uname" required lay-verify="required"
                                           autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid" style="color: #FF5722">${namewarn}</div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_sex" class="layui-form-label">性别</label>
                                <div class="layui-input-inline" lay-filter="usex">
                                    <select id="L_sex" name="usex">
                                        <option value="m">男</option>
                                        <option value="w">女</option>
                                        <option value="s" selected>保密</option>
                                    </select>
                                </div>
                                <div class="layui-form-mid" style="color: #FF5722">${sexwarn}</div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_tel" class="layui-form-label">电话</label>
                                <div class="layui-input-inline">
                                    <input type="text" id="L_tel" name="utel"
                                           autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid" style="color: #FF5722">${telwarn}</div>
                            </div>
                            <div class="layui-form-item">
                                <label for="L_pass" class="layui-form-label">密码</label>
                                <div class="layui-input-inline">
                                    <input type="password" id="L_pass" name="upass" required lay-verify="required"
                                           autocomplete="off" class="layui-input">
                                </div>
                                <div class="layui-form-mid" style="color: #FF5722">${passwarn}</div>
                                <div class="layui-form-mid layui-word-aux">6到18个字符，仅限英文和数字</div>
                            </div>
                            <div class="layui-form-item">
                                <button type="submit" class="layui-btn" lay-filter="*" lay-submit>立即注册</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="fly-footer">
    <p><a href="#" target="_blank">加帕里集市</a> 2020 &copy; <a href="#" target="_blank">Haoo</a></p>
</div>

<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    layui.use('form', function () {
        var form = layui.form;
        form.render('select', 'sex');
        //各种基于事件的操作，下面会有进一步介绍
        //form.render('组件名','lay-filter名')
    });
</script>
<script>
    layui.cache.page = 'user';
    layui.cache.user = {
        username: '游客'
        , uid: -1
        , avatar: '../../../res/images/avatar/00.jpg'
        , experience: 83
        , sex: '男'
    };
    layui.config({
        version: "3.0.0"
        , base: '../../../res/mods/'
    }).extend({
        fly: 'index'
    }).use('fly');
</script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<%--<script>
    function cno(){
        $.post({
            url:"${pageContext.request.contextPath}/a3",
            data:{'cno':$("#L_no").val()},
            success:function (data) {
                if (data.toString()=='OK'){
                    $("#userInfo").css("color","green");
                }else {
                    $("#userInfo").css("color","red");
                }
                $("#userInfo").html(data);
            }
        });
    }
    function cname(){
        $.post({
            url:"${pageContext.request.contextPath}/a3",
            data:{'pwd':$("#pwd").val()},
            success:function (data) {
                if (data.toString()=='OK'){
                    $("#pwdInfo").css("color","green");
                }else {
                    $("#pwdInfo").css("color","red");
                }
                $("#pwdInfo").html(data);
            }
        });
    }
</script>--%>
</body>
</html>