<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>欢迎来到加帕里集市</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="keywords" content="校园，二手交易，加帕里，论坛">
    <meta name="description" content="校园二手物品交流平台——加帕里集市">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/global.css">
</head>
<body>

<div class="fly-header layui-bg-black">
    <div class="layui-container">
        <a class="fly-logo" href="${pageContext.request.contextPath}/index">
            <img src="${pageContext.request.contextPath}/static/images/logo.png" alt="layui">
        </a>

        <ul class="layui-nav fly-nav-user">
            <c:choose>
                <c:when test="${empty user}">
                    <!-- 未登入的状态 -->
                    <li class="layui-nav-item">
                        <a class="iconfont icon-touxiang layui-hide-xs"
                           href="${pageContext.request.contextPath}/user/login"></a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="${pageContext.request.contextPath}/user/login">登入</a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="${pageContext.request.contextPath}/user/regist">注册</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <!-- 登入后的状态 -->
                    <li class="layui-nav-item">
                        <a class="fly-nav-avatar" href="javascript:;">
                            <cite class="layui-hide-xs">${user.getUname()}</cite>
                            <c:if test="${user.getUauth() == 1}"><i
                                    class="layui-badge fly-badge-vip layui-hide-xs">管理员</i></c:if>
                            <img src="${pageContext.request.contextPath}/user/getpic?uid=${user.getUid()}">
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a href="${pageContext.request.contextPath}/user/set"><i class="layui-icon">&#xe620;</i>基本设置</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/message"><i
                                    class="iconfont icon-tongzhi" style="top: 4px;"></i>消息</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/key"><i class="layui-icon">&#xe66e;</i>我的关注</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/home"><i class="layui-icon"
                                                                                          style="margin-left: 2px; font-size: 22px;">&#xe68e;</i>我的主页</a>
                            </dd>
                            <hr style="margin: 5px 0;">
                            <dd><a href="${pageContext.request.contextPath}/user/logout"
                                   style="text-align: center;">退出</a></dd>
                        </dl>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</div>

<div class="layui-container fly-marginTop fly-user-main">
    <ul class="layui-nav layui-nav-tree layui-inline" lay-filter="user">
        <li class="layui-nav-item">
            <a href="${pageContext.request.contextPath}/user/home">
                <i class="layui-icon">&#xe609;</i>
                我的主页
            </a>
        </li>
        <%--
            <li class="layui-nav-item">
              <a href="index.jsp">
                <i class="layui-icon">&#xe612;</i>
                用户中心
              </a>
            </li>
            --%>
        <li class="layui-nav-item layui-this">
            <a href="${pageContext.request.contextPath}/user/set">
                <i class="layui-icon">&#xe620;</i>
                基本设置
            </a>
        </li>
        <li class="layui-nav-item">
            <a href="${pageContext.request.contextPath}/user/key">
                <i class="layui-icon">&#xe670;</i>
                我的关注
            </a>
        </li>
        <li class="layui-nav-item">
            <a href="${pageContext.request.contextPath}/user/message">
                <i class="layui-icon">&#xe611;</i>
                我的消息
            </a>
        </li>
    </ul>

    <div class="site-tree-mobile layui-hide">
        <i class="layui-icon">&#xe602;</i>
    </div>
    <div class="site-mobile-shade"></div>

    <div class="site-tree-mobile layui-hide">
        <i class="layui-icon">&#xe602;</i>
    </div>
    <div class="site-mobile-shade"></div>


    <div class="fly-panel fly-panel-user" pad20>
        <div class="layui-tab layui-tab-brief" lay-filter="user">
            <ul class="layui-tab-title" id="LAY_mine">
                <li class="layui-this" lay-id="info">我的资料</li>
                <li lay-id="avatar">头像</li>
                <li lay-id="pass">密码</li>
            </ul>
            <div class="layui-tab-content" style="padding: 20px 0;">
                <div class="layui-form layui-form-pane layui-tab-item layui-show">
                    <form method="post" action="${pageContext.request.contextPath}/user/upload1">
                        <input type="hidden" id="L_uid" name="uid" value="${user.getUid()}">
                        <div class="layui-form-item">
                            <label for="L_uno" class="layui-form-label">账号</label>
                            <div class="layui-input-inline">
                                <input type="text" id="L_uno" name="uno" required lay-verify="required"
                                       autocomplete="off" value="${user.getUno()}" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label for="L_uname" class="layui-form-label">昵称</label>
                            <div class="layui-input-inline">
                                <input type="text" id="L_uname" name="uname" required lay-verify="required"
                                       autocomplete="off" value="${user.getUname()}" class="layui-input">
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <label for="L_uname" class="layui-form-label">性别</label>
                            <div class="layui-input-inline">
                                <input type="radio" name="usex" value="m"
                                       <c:if test="${user.getUsex() eq '男'}">checked</c:if> title="男">
                                <input type="radio" name="usex" value="w"
                                       <c:if test="${user.getUsex() eq '女'}">checked</c:if> title="女">
                                <input type="radio" name="usex" value="s"
                                       <c:if test="${user.getUsex() eq '保密'}">checked</c:if> title="保密">
                            </div>
                        </div>
                        <div class="layui-form-item layui-form-text">
                            <label for="L_uother" class="layui-form-label">备注</label>
                            <div class="layui-input-block">
                                    <textarea placeholder="随便写些什么刷下存在感" id="L_uother" name="uother"
                                              autocomplete="off"
                                              class="layui-textarea" style="height: 80px;"><c:if
                                            test="${user.getUother() != null}">${user.getUother()}</c:if></textarea>
                            </div>
                        </div>
                        <div class="layui-form-item">
                            <button class="layui-btn" key="set-mine" lay-filter="*" lay-submit type="submit">确认修改
                            </button>
                        </div>
                    </form>
                </div>


                <div class="layui-form layui-form-pane layui-tab-item">
                    <div class="layui-form-item">
                        <form action="${pageContext.request.contextPath}/user/upload2" enctype="multipart/form-data"
                              method="post">
                            <div class="avatar-add">
                                <p>建议尺寸168*168，支持jpg、png、gif，最大不能超过50KB</p>
                                <img src="${pageContext.request.contextPath}/user/getpic?uid=${user.getUid()}">
                                <span class="loading"></span>
                                <%--<button type="button" class="layui-btn upload-img" id="L_avatar">
                                    <i class="layui-icon">&#xe67c;</i>上传头像
                                </button>--%>
                            </div>
                            <div class="layui-form-item">
                                <ul class="layui-form layui-form-pane" style="margin: 20px;">
                                    <li class="layui-form-item">
                                        <input type="file" name="file" class="layui-btn"/>
                                    </li>
                                    <li class="layui-form-item">
                                        <input type="submit" class="layui-btn" value="上传"/>
                                    </li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="layui-form layui-form-pane layui-tab-item">
                    <form action="${pageContext.request.contextPath}/user/repass" method="post">
                        <div class="layui-form-item">
                            <label for="L_nowpass" class="layui-form-label">当前密码</label>
                            <div class="layui-input-inline">
                                <input type="password" id="L_nowpass" name="nowpass" required lay-verify="required"
                                       autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-form-mid" style="color: #FF5722">${nowpasswarn}</div>
                        </div>
                        <div class="layui-form-item">
                            <label for="L_pass" class="layui-form-label">新密码</label>
                            <div class="layui-input-inline">
                                <input type="password" id="L_pass" name="pass" required lay-verify="required"
                                       autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-form-mid" style="color: #FF5722">${passwarn}</div>
                            <div class="layui-form-mid layui-word-aux">6到16个字符</div>
                        </div>
                        <div class="layui-form-item">
                            <label for="L_repass" class="layui-form-label">确认密码</label>
                            <div class="layui-input-inline">
                                <input type="password" id="L_repass" name="repass" required lay-verify="required"
                                       autocomplete="off" class="layui-input">
                            </div>
                            <div class="layui-form-mid" style="color: #FF5722">${repasswarn}</div>

                        </div>
                        <div class="layui-form-item">
                            <button class="layui-btn" key="set-mine" lay-filter="*" lay-submit>确认修改</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="fly-footer">
    <p><a href="#" target="_blank">加帕里集市</a> 2020 &copy; <a href="#" target="_blank">Haoo</a></p>
</div>

<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script>
    layui.use('form', function () {
        var form = layui.form;
        form.render('select', 'sex');
        //各种基于事件的操作，下面会有进一步介绍
        //form.render('组件名','lay-filter名')
    });
</script>
<script>
    layui.cache.page = '';
    layui.cache.user = {
        username: '${user.getUname()}'
        , uid: '${user.getUno()}'
        , avatar: '${pageContext.request.contextPath}/user/getpic?uid=${user.getUid()}'
        , sex: '${user.getUsex()}'
    };
    layui.config({
        version: "3.0.0"
        , base: '${pageContext.request.contextPath}/static/mods/' //这里实际使用时，建议改成绝对路径
    }).extend({
        fly: 'index'
    }).use('fly');
</script>
</body>
</html>