<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>我的消息</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="keywords" content="校园，二手交易，加帕里，论坛">
    <meta name="description" content="校园二手物品交流平台——加帕里集市">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/global.css">
</head>
<body>

<div class="fly-header layui-bg-black">
    <div class="layui-container">
        <a class="fly-logo" href="${pageContext.request.contextPath}/index">
            <img src="${pageContext.request.contextPath}/static/images/logo.png" alt="layui">
        </a>

        <ul class="layui-nav fly-nav-user">
            <c:choose>
                <c:when test="${empty user}">
                    <!-- 未登入的状态 -->
                    <li class="layui-nav-item">
                        <a class="iconfont icon-touxiang layui-hide-xs"
                           href="${pageContext.request.contextPath}/user/login"></a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="${pageContext.request.contextPath}/user/login">登入</a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="${pageContext.request.contextPath}/user/regist">注册</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <!-- 登入后的状态 -->
                    <li class="layui-nav-item">
                        <a class="fly-nav-avatar" href="javascript:;">
                            <cite class="layui-hide-xs">${user.getUname()}</cite>
                            <c:if test="${user.getUauth() == 1}"><i
                                    class="layui-badge fly-badge-vip layui-hide-xs">管理员</i></c:if>
                            <img src="${pageContext.request.contextPath}/user/getpic?uid=${user.getUid()}">
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a href="${pageContext.request.contextPath}/user/set"><i class="layui-icon">&#xe620;</i>基本设置</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/message"><i
                                    class="iconfont icon-tongzhi" style="top: 4px;"></i>消息</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/key"><i class="layui-icon">&#xe66e;</i>我的关注</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/home"><i class="layui-icon"
                                                                                          style="margin-left: 2px; font-size: 22px;">&#xe68e;</i>我的主页</a>
                            </dd>
                            <hr style="margin: 5px 0;">
                            <dd><a href="${pageContext.request.contextPath}/user/logout"
                                   style="text-align: center;">退出</a></dd>
                        </dl>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</div>

<div class="layui-container fly-marginTop fly-user-main">
    <ul class="layui-nav layui-nav-tree layui-inline" lay-filter="user">
        <li class="layui-nav-item">
            <a href="${pageContext.request.contextPath}/user/home">
                <i class="layui-icon">&#xe609;</i>
                我的主页
            </a>
        </li>
        <%--
            <li class="layui-nav-item">
              <a href="index.jsp">
                <i class="layui-icon">&#xe612;</i>
                用户中心
              </a>
            </li>
            --%>
        <li class="layui-nav-item">
            <a href="${pageContext.request.contextPath}/user/set">
                <i class="layui-icon">&#xe620;</i>
                基本设置
            </a>
        </li>
        <li class="layui-nav-item">
            <a href="${pageContext.request.contextPath}/user/key">
                <i class="layui-icon">&#xe670;</i>
                我的关注
            </a>
        </li>
        <li class="layui-nav-item  layui-this">
            <a href="${pageContext.request.contextPath}/user/message">
                <i class="layui-icon">&#xe611;</i>
                我的消息
            </a>
        </li>
    </ul>

    <div class="site-tree-mobile layui-hide">
        <i class="layui-icon">&#xe602;</i>
    </div>
    <div class="site-mobile-shade"></div>

    <div class="site-tree-mobile layui-hide">
        <i class="layui-icon">&#xe602;</i>
    </div>
    <div class="site-mobile-shade"></div>


    <div class="fly-panel fly-panel-user" pad20>
        <div class="layui-tab layui-tab-brief" lay-filter="user" id="LAY_msg" style="margin-top: 15px;">
            <a href="${pageContext.request.contextPath}/user/message/readall">
                <button class="layui-btn layui-btn-danger" id="LAY_delallmsg">全部已读</button>
            </a>
            <div id="LAY_minemsg" style="margin-top: 10px;">
                <!--<div class="fly-none">您暂时没有最新消息</div>-->
                <ul class="mine-msg">
                    <c:forEach var="r" items="${requestScope.get('commentreads')}">
                        <li data-id="123">
                            <blockquote class="layui-elem-quote">
                                <a href="${pageContext.request.contextPath}/user/home?uid=${r.getUname()}"
                                   target="_blank"><cite>${r.getUname()}</cite></a>回复了你的
                                <a target="_blank"
                                   href="${pageContext.request.contextPath}/item/detail?iid=${r.getIidp()}">
                                    <c:choose>
                                        <c:when test="${empty r.getCidp()}">
                                            <cite>${r.getIunamep()}</cite>
                                        </c:when>
                                        <c:otherwise>
                                            <cite>${r.getCcontentp()}</cite>
                                        </c:otherwise>
                                    </c:choose>
                                </a>说：${r.getCcontent()}

                            </blockquote>
                            <p><span>${r.timeToNow(r.getCtime())}前</span>
                                <c:if test="${r.getCread()==0}"><a
                                        href="${pageContext.request.contextPath}/user/message/read?cid=${r.getCid()}&gid=-1"
                                        class="layui-btn layui-btn-small layui-btn-danger fly-delete">已读</a></c:if>
                            </p>
                        </li>
                    </c:forEach>

                    <c:forEach var="r" items="${requestScope.get('greatreads')}">
                        <li data-id="123">
                            <blockquote class="layui-elem-quote">
                                <a href="${pageContext.request.contextPath}/user/home?uid=${r.getUname()}"
                                   target="_blank"><cite>${r.getUname()}</cite></a>赞了你的
                                <a target="_blank"
                                   href="${pageContext.request.contextPath}/item/detail?iid=${r.getIidp()}">
                                    <c:choose>
                                        <c:when test="${empty r.getCidp()}">
                                            <cite>${r.getIunamep()}</cite>
                                        </c:when>
                                        <c:otherwise>
                                            <cite>${r.getCcontentp()}</cite>
                                        </c:otherwise>
                                    </c:choose>
                                </a>

                            </blockquote>
                            <p><span>${r.timeToNow(r.getGtime())}前</span>
                                <c:if test="${r.getGread()==0}"><a
                                        href="${pageContext.request.contextPath}/user/message/read?cid=-1&gid=${r.getGid()}"
                                        class="layui-btn layui-btn-small layui-btn-danger fly-delete">已读</a></c:if>
                            </p>
                        </li>
                    </c:forEach>

                    <%--<li data-id="123">
                        <blockquote class="layui-elem-quote">
                            系统消息：欢迎使用 layui
                        </blockquote>
                        <p><span>1小时前</span><a href="javascript:;"
                                               class="layui-btn layui-btn-small layui-btn-danger fly-delete">删除</a></p>
                    </li>--%>
                </ul>
            </div>
        </div>
    </div>

</div>


<div class="fly-footer">
    <p><a href="#" target="_blank">加帕里集市</a> 2020 &copy; <a href="#" target="_blank">Haoo</a></p>
</div>

<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    layui.cache.page = '';
    layui.cache.user = {
        username: '${user.getUname()}'
        , uid: '${user.getUno()}'
        , avatar: '${pageContext.request.contextPath}/user/getpic?uid=${user.getUid()}'
        , sex: '${user.getUsex()}'
    };
    layui.config({
        version: "3.0.0"
        , base: '${pageContext.request.contextPath}/static/mods/' //这里实际使用时，建议改成绝对路径
    }).extend({
        fly: 'index'
    }).use('fly');
</script>

</body>
</html>