<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>登入</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <meta name="keywords" content="校园，二手交易，加帕里，论坛">
  <meta name="description" content="校园二手物品交流平台——加帕里集市">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/global.css">
</head>
<body>

<div class="fly-header layui-bg-black">
  <div class="layui-container">
    <a class="fly-logo" href="${pageContext.request.contextPath}/index">
      <img src="${pageContext.request.contextPath}/static/images/logo.png" alt="layui">
    </a>
    
    <ul class="layui-nav fly-nav-user">
      <!-- 未登入的状态 -->
      <li class="layui-nav-item">
        <a class="iconfont icon-touxiang layui-hide-xs" href="${pageContext.request.contextPath}/user/login""></a>
      </li>
      <li class="layui-nav-item">
        <a href="${pageContext.request.contextPath}/user/login"">登入</a>
      </li>
      <li class="layui-nav-item">
        <a href="${pageContext.request.contextPath}/user/regist">注册</a>
      </li>
    </ul>
  </div>
</div>

<div class="layui-container fly-marginTop">
  <div class="fly-panel fly-panel-user" pad20>
    <div class="layui-tab layui-tab-brief" lay-filter="user">
      <ul class="layui-tab-title">
        <li class="layui-this">登入</li>
        <li><a href="${pageContext.request.contextPath}/user/regist">注册</a></li>
      </ul>
      <div class="layui-form layui-tab-content" id="LAY_ucm" style="padding: 20px 0;">
        <div class="layui-tab-item layui-show">
          <div class="layui-form layui-form-pane">
            <form method="post" action="${pageContext.request.contextPath}/user/login">
              <div class="layui-form-item">
                <label for="L_no" class="layui-form-label">账户</label>
                <div class="layui-input-inline">
                  <input type="text" id="L_no" name="uno" required lay-verify="required"
                         autocomplete="off" class="layui-input">
                </div>
                <div class="layui-form-mid" style="color: #FF5722">${nowarn}</div>
              </div>
              <div class="layui-form-item">
                <label for="L_pass" class="layui-form-label">密码</label>
                <div class="layui-input-inline">
                  <input type="password" id="L_pass" name="upass" required lay-verify="required" autocomplete="off" class="layui-input">
                </div>
                <div class="layui-form-mid" style="color: #FF5722">${passwarn}</div>
              </div>

              <div class="layui-form-item">
                <button class="layui-btn" lay-filter="*" lay-submit>立即登录</button>
                <!--
                <span style="padding-left:20px;">
                  <a href="forget.html">忘记密码？</a>
                </span>
                -->
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="fly-footer">
  <p><a href="#" target="_blank">加帕里集市</a> 2020 &copy; <a href="#" target="_blank">Haoo</a></p>
</div>

<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
  layui.cache.page = '';
  layui.cache.user = {
    username: '${user.getUname()}'
    , uid: '${user.getUno()}'
    , avatar: '${pageContext.request.contextPath}/static/images/avatar/${user.getUid()}.png'
    , sex: '${user.getUsex()}'
  };
  layui.config({
    version: "3.0.0"
    , base: '${pageContext.request.contextPath}/static/mods/' //这里实际使用时，建议改成绝对路径
  }).extend({
    fly: 'index'
  }).use('fly');
</script>

</body>
</html>