<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>加帕里集市后台</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">加帕里集市后台</div>
        <!-- 头部区域（可配合layui已有的水平导航） -->
        <%--<ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item"><a href="">控制台</a></li>
            <li class="layui-nav-item"><a href="">商品管理</a></li>
            <li class="layui-nav-item"><a href="">用户</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">其它系统</a>
                <dl class="layui-nav-child">
                    <dd><a href="">邮件管理</a></dd>
                    <dd><a href="">消息管理</a></dd>
                    <dd><a href="">授权管理</a></dd>
                </dl>
            </li>
        </ul>--%>
        <ul class="layui-nav fly-nav-user layui-layout-right">
            <li class="layui-nav-item">
                <a href="javascript:;">
                    ${user.getUname()}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="${pageContext.request.contextPath}/user/set">基本设置</a></dd>
                    <dd><a href="${pageContext.request.contextPath}/user/message">消息</a></dd>
                    <dd><a href="${pageContext.request.contextPath}/user/home">我的主页</a></dd>
                    <dd><a href="${pageContext.request.contextPath}/user/logout">退出</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/index">回到首页</a></li>
        </ul>
    </div>

    <div class="layui-side layui-bg-black">
        <div class="layui-side-scroll">
            <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
            <ul class="layui-nav layui-nav-tree" lay-filter="test">
                <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/admin">序</a></li>
                <li class="layui-nav-item layui-nav-itemed">
                    <a class="" href="javascript:;">帖</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${pageContext.request.contextPath}/admin/item">所有帖</a></dd>
                        <dd><a href="${pageContext.request.contextPath}/admin/item/top">置顶帖</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">用户</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${pageContext.request.contextPath}/admin/user">所有用户</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">留言&评论</a>
                    <dl class="layui-nav-child">
                        <dd><a href="${pageContext.request.contextPath}/admin/comm">所有留言&评论</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item"><a href="${pageContext.request.contextPath}/admin">结</a></li>
            </ul>
        </div>
    </div>

    <div class="layui-body">
        <!-- 内容主体区域 -->

        <div class="layui-row layui-col-space10" style="padding: 15px;">
            <div class="layui-col-md6">
                <div class="layui-collapse">

                    <div class="layui-colla-item">
                        <h2 class="layui-colla-title">欢迎加帕里集市——后台管理系统</h2>
                        <div class="layui-colla-content layui-show">
                            <p><span class="layui-badge-dot layui-bg-black"></span> 这里是加帕里集市的后台管理系统</p>
                            <p><span class="layui-badge-dot layui-bg-black"></span> 主要针对“帖、用户、留言&评论”进行管理。</p>
                        </div>
                    </div>
                    <div class="layui-colla-item">
                        <h2 class="layui-colla-title">帖——管理</h2>
                        <div class="layui-colla-content layui-show">
                            <p><span class="layui-badge-dot layui-bg-black"></span> 浏览帖的详细信息</p>
                            <p><span class="layui-badge-dot layui-bg-black"></span> 可以针对帖的相关信息进行精确的检索</p>
                            <p><span class="layui-badge-dot layui-bg-black"></span> 对帖可以进行单独或批量的“删除”，“修改”，“置顶”等操作</p>
                        </div>
                    </div>
                    <div class="layui-colla-item">
                        <h2 class="layui-colla-title">用户——管理</h2>
                        <div class="layui-colla-content layui-show">
                            <p><span class="layui-badge-dot layui-bg-black"></span> 浏览用户的详细信息</p>
                            <p><span class="layui-badge-dot layui-bg-black"></span> 可以针对用户的相关信息进行精确的检索</p>
                            <p><span class="layui-badge-dot layui-bg-black"></span> 对用户可以进行单独或批量的“删除”，“修改”，“修改权限”等操作</p>
                        </div>
                    </div>
                    <div class="layui-colla-item">
                        <h2 class="layui-colla-title">留言&评论——管理</h2>
                        <div class="layui-colla-content layui-show">
                            <p><span class="layui-badge-dot layui-bg-black"></span> 浏览留言的详细信息</p>
                            <p><span class="layui-badge-dot layui-bg-black"></span> 可以针对留言的相关信息进行精确的检索</p>
                            <p><span class="layui-badge-dot layui-bg-black"></span> 对留言可以进行单独或批量的“删除”，“修改”等操作</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="layui-footer">
        <p><a href="#" target="_blank">加帕里集市</a> 2020 &copy; <a href="#" target="_blank">Haoo</a></p>
    </div>

    <script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
    <script>
        //JavaScript代码区域
        layui.use('element', function () {
            var element = layui.element;

        });
    </script>
</div>
</body>
</html>