<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <title>欢迎来到加帕里集市</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="keywords" content="校园，二手交易，加帕里，论坛">
    <meta name="description" content="校园二手物品交流平台——加帕里集市">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/layui/css/layui.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/static/css/global.css">
</head>
<body>

<div class="fly-header layui-bg-black">
    <div class="layui-container">
        <a class="fly-logo" href="${pageContext.request.contextPath}/index">
            <img src="${pageContext.request.contextPath}/static/images/logo.png" alt="layui">
        </a>

        <ul class="layui-nav fly-nav-user">
            <c:choose>
                <c:when test="${empty user}">
                    <!-- 未登入的状态 -->
                    <li class="layui-nav-item">
                        <a class="iconfont icon-touxiang layui-hide-xs"
                           href="${pageContext.request.contextPath}/user/login"></a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="${pageContext.request.contextPath}/user/login">登入</a>
                    </li>
                    <li class="layui-nav-item">
                        <a href="${pageContext.request.contextPath}/user/regist">注册</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <!-- 登入后的状态 -->
                    <li class="layui-nav-item">
                        <a class="fly-nav-avatar" href="javascript:;">
                            <cite class="layui-hide-xs">${user.getUname()}</cite>
                            <c:if test="${user.getUauth() == 1}"><i
                                    class="layui-badge fly-badge-vip layui-hide-xs">管理员</i></c:if>
                            <img src="${pageContext.request.contextPath}/user/getpic?uid=${user.getUid()}">
                        </a>
                        <dl class="layui-nav-child">
                            <dd><a href="${pageContext.request.contextPath}/user/set"><i class="layui-icon">&#xe620;</i>基本设置</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/message"><i
                                    class="iconfont icon-tongzhi" style="top: 4px;"></i>消息</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/key"><i class="layui-icon">&#xe66e;</i>我的关注</a>
                            </dd>
                            <dd><a href="${pageContext.request.contextPath}/user/home"><i class="layui-icon"
                                                                                          style="margin-left: 2px; font-size: 22px;">&#xe68e;</i>我的主页</a>
                            </dd>
                            <hr style="margin: 5px 0;">
                            <dd><a href="${pageContext.request.contextPath}/user/logout"
                                   style="text-align: center;">退出</a></dd>
                        </dl>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </div>
</div>

<div class="fly-panel fly-column">
    <div class="layui-container">
        <%--首页（显示所有消息）、
        公告（显示所有公告）、
        二手（按照时间，显示所有二手帖）、
        图文（按照时间，显示所有图文帖）、
        我的关注（显示包含关键词的所有内容，添加删除关键词）
        小红点<span class="layui-badge-dot"></span>--%>
        <ul class="layui-clear">
            <li class="<c:if test="${empty itype}">layui-hide-xs layui-this</c:if>"><a
                    href="${pageContext.request.contextPath}/index">首页</a></li>
            <li class="<c:if test="${itype==7}">layui-hide-xs layui-this</c:if>"><a
                    href="${pageContext.request.contextPath}/index/type?type=热议">热议</a></li>
            <li class="<c:if test="${itype==1}">layui-hide-xs layui-this</c:if>"><a
                    href="${pageContext.request.contextPath}/index/type?type=公告">公告</a></li>
            <li class="<c:if test="${itype==2}">layui-hide-xs layui-this</c:if>"><a
                    href="${pageContext.request.contextPath}/index/type?type=图文">图文</a></li>
            <li class="<c:if test="${itype==3||itype==4||itype==5}">layui-hide-xs layui-this</c:if>"><a
                    href="${pageContext.request.contextPath}/index/type?type=二手">二手</a></li>
            <li class="<c:if test="${itype==6}">layui-hide-xs layui-this</c:if>"><a
                    href="${pageContext.request.contextPath}/index/type?type=捐赠">捐赠</a></li>
            <c:if test="${user.getUauth() == 1}"><i
                <li class="<c:if test="${itype==6}">layui-hide-xs layui-this</c:if>"><a
                        href="${pageContext.request.contextPath}/admin">admin</a></li>
            </c:if>
            <c:if test="${not empty user}">
                <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block">
                    <span class="fly-mid"></span>
                </li>
                <!-- 用户登入后显示 -->
                <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block"><a
                        href="${pageContext.request.contextPath}/user/home">我的发布</a></li>
                <li class="layui-hide-xs layui-hide-sm layui-show-md-inline-block"><a
                        href="${pageContext.request.contextPath}/focus">我的关注</a></li>
            </c:if>
        </ul>

        <div class="fly-column-right layui-hide-xs">
            <span class="fly-search"><i class="layui-icon"></i></span>
            <a href="${pageContext.request.contextPath}/item/add" class="layui-btn">发布</a>
        </div>
        <div class="layui-hide-sm layui-show-xs-block"
             style="margin-top: -10px; padding-bottom: 10px; text-align: center;">
            <a href="${pageContext.request.contextPath}/item/add" class="layui-btn">发布</a>
        </div>
    </div>
</div>

<%--浏览prat--%>
<div class="layui-container">
    <div class="layui-row layui-col-space15">
        <div class="layui-col-md8">
            <%--置顶部分--%>

            <c:if test="${not empty toplist}">
                <div class="fly-panel">
                    <div class="fly-panel-title fly-filter">
                        <a>置顶</a>
                    </div>

                    <ul class="fly-list">
                        <c:forEach var="ti" items="${requestScope.get('toplist')}">
                            <li>
                                    <%--头像--%>
                                <a href="#" class="fly-avatar">
                                    <img src="${pageContext.request.contextPath}/user/getpic?uid=${ti.getUid()}"
                                         alt="${ti.getUname()}">
                                </a>
                                    <%--分类和标题--%>
                                <h2>
                                    <c:choose>
                                        <c:when test="${ti.getItype() == 1}"><a class="layui-badge"> 公告 </a></c:when>
                                        <c:when test="${ti.getItype() == 2}"><a class="layui-badge"> 图文 </a></c:when>
                                        <c:otherwise><a class="layui-badge"> 二手 </a></c:otherwise>
                                    </c:choose>
                                    <a href="${pageContext.request.contextPath}/item/detail?iid=${ti.getIid()}">${ti.getIname()}</a>
                                </h2>
                                <div class="fly-list-info">
                                        <%--作者信息--%>
                                    <a href="${pageContext.request.contextPath}/user/neighbor?uid=${ti.getUid()}" link>
                                        <cite>${ti.getUname()}</cite>
                                        <c:choose>
                                            <c:when test="${ti.getUauth() == 1}"><i class="layui-badge fly-badge-vip"
                                                                                    title="管理员">管理员</i></c:when>
                                        </c:choose>

                                    </a>
                                        <%--时间--%>
                                    <span>${ti.getItime()}前</span>
                                        <%--赞情况--%>
                                    <span class="fly-list-kiss layui-hide-xs" title="赞"><i
                                            class="iconfont icon-zan"></i> ${ti.getGreat()}</span>
                                        <%--已经结束--%>
                                    <c:choose>
                                        <c:when test="${ti.getItype() == 4}"><span
                                                class="layui-badge fly-badge-accept layui-hide-xs">已结束</span></c:when>
                                    </c:choose>
                                        <%--评论数量--%>
                                    <span class="fly-list-nums">
                                    <i class="iconfont icon-pinglun1" title="评论"></i> ${ti.getComment()}
                                    <i class="iconfont" title="浏览量">&#xe60b;</i> ${ti.getIbrowse()}
                                </span>

                                </div>
                                <div class="fly-list-badge">
                                        <%--置顶信息
                                        <span class="layui-badge layui-bg-black">置顶</span>--%>
                                </div>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </c:if>

            <%--普通帖子--%>
            <div class="fly-panel" style="margin-bottom: 0;">
                <%--分类--%>
                <div class="fly-panel-title fly-filter">
                    <a href="${pageContext.request.contextPath}/index/type?type=二手"
                       class="<c:if test="${itype==5}">layui-this</c:if>">综合</a>
                    <c:if test="${itype==3||itype==4||itype==5}">
                        <span class="fly-mid"></span>
                        <a href="${pageContext.request.contextPath}/index/type?type=未结束"
                           class="<c:if test="${itype==3}">layui-this</c:if>">未结束</a>
                        <span class="fly-mid"></span>
                        <a href="${pageContext.request.contextPath}/index/type?type=已结束"
                           class="<c:if test="${itype==4}">layui-this</c:if>">已结束</a>
                    </c:if>
                    <%--<span class="fly-filter-right layui-hide-xs">
                        <a href="" class="layui-this">按最新</a>
                        <span class="fly-mid"></span>
                        <a href="">按热议</a>
                    </span>--%>
                </div>
                <ul class="fly-list" id="list">
                    <c:forEach var="i" items="${requestScope.get('list')}">

                        <li>
                                <%--头像--%>
                            <a href="#" class="fly-avatar">
                                <img src="${pageContext.request.contextPath}/user/getpic?uid=${i.getUid()}"
                                     alt="${i.getUname()}">
                            </a>
                                <%--分类和标题--%>
                            <h2>
                                <c:choose>
                                    <c:when test="${i.getItype() == 1}"><a class="layui-badge"> 公告 </a></c:when>
                                    <c:when test="${i.getItype() == 2}"><a class="layui-badge"> 图文 </a></c:when>
                                    <c:otherwise><a class="layui-badge"> 二手 </a></c:otherwise>
                                </c:choose>
                                <a href="${pageContext.request.contextPath}/item/detail?iid=${i.getIid()}">${i.getIname()}</a>
                            </h2>
                            <div class="fly-list-info">
                                    <%--作者信息--%>
                                <a href="${pageContext.request.contextPath}/user/neighbor?uid=${i.getUid()}" link>
                                    <cite>${i.getUname()}</cite>
                                    <c:choose>
                                        <c:when test="${i.getUauth() == 1}"><i class="layui-badge fly-badge-vip"
                                                                               title="管理员">管理员</i></c:when>
                                    </c:choose>
                                </a>
                                    <%--时间--%>
                                <span>${i.getItime()}前</span>

                                    <%--点赞--%>
                                <span class="fly-list-kiss layui-hide-xs" title="赞"><i
                                        class="iconfont icon-zan"></i> ${i.getGreat()}</span>
                                    <%--是否已经结束--%>
                                <c:choose>
                                    <c:when test="${i.getItype() == 4}"><span
                                            class="layui-badge fly-badge-accept layui-hide-xs">已结束</span></c:when>
                                </c:choose>
                                    <%--评论数量--%>
                                <span class="fly-list-nums">
                                    <i class="iconfont icon-pinglun1" title="评论"></i> ${i.getComment()}
                                    <i class="iconfont" title="浏览量">&#xe60b;</i> ${i.getIbrowse()}
                                </span>
                            </div>
                            <div class="fly-list-badge">
                                <!--<span class="layui-badge layui-bg-red">精帖</span>-->
                            </div>
                        </li>
                    </c:forEach>
                </ul>
                <%--更多--%>
                <div style="text-align: center">
                    <div class="laypage-main">
                        <a class="laypage-next" onclick="">更多</a>
                    </div>
                </div>

            </div>
        </div>


        <div class="layui-col-md4">

            <div class="fly-panel fly-rank fly-rank-reply" id="LAY_replyRank">
                <h3 class="fly-panel-title">本周活跃</h3>
                <dl>
                    &lt;!&ndash;<i class="layui-icon fly-loading">&#xe63d;</i>&ndash;&gt;
                    <c:forEach var="hu" items="${requestScope.get('hotuser')}">
                        <dd>
                            <a href="user/home.jsp">
                                <img src="${pageContext.request.contextPath}/user/getpic?uid=${hu.getUid()}"><cite>${hu.getUname()}</cite><%--<i>106次回答</i>--%>
                            </a>
                        </dd>
                    </c:forEach>
                    <%--无数据时--%>
                    <c:if test="${empty hotuser}">
                        <div class="fly-none">没有相关数据</div>
                    </c:if>
                </dl>
            </div>

            <dl class="fly-panel fly-list-one">
                <dt class="fly-panel-title">本周热点</dt>
                <c:forEach var="hi" items="${requestScope.get('hotitem')}">
                    <dd>
                        <a href="${pageContext.request.contextPath}/item/detail?iid=${hi.getIid()}">${hi.getIname()}</a>
                            <%--<span><i class="iconfont icon-pinglun1"></i> 16</span>--%>
                    </dd>
                </c:forEach>
                <%--无数据时--%>
                <c:if test="${empty hotitem}">
                    <div class="fly-none">没有相关数据</div>
                </c:if>
            </dl>
        </div>
    </div>
</div>

<div class="fly-footer">
    <p><a href="#" target="_blank">加帕里集市</a> 2020 &copy; <a href="#" target="_blank">Haoo</a></p>
</div>

<script src="${pageContext.request.contextPath}/static/layui/layui.js"></script>
<script>
    layui.cache.page = '';
    layui.cache.user = {
        username: '${user.getUname()}'
        , uid: '${user.getUno()}'
        , avatar: '${pageContext.request.contextPath}/user/getpic?uid=${user.getUid()}'
        , sex: '${user.getUsex()}'
    };
    layui.config({
        version: "3.0.0"
        , base: '${pageContext.request.contextPath}/static/mods/' //这里实际使用时，建议改成绝对路径
    }).extend({
        fly: 'index'
    }).use('fly');
</script>


<script type="text/javascript">
    var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    document.write(unescape("%3Cspan id='cnzz_stat_icon_30088308'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol +
        "w.cnzz.com/c.php%3Fid%3D30088308' type='text/javascript'%3E%3C/script%3E"));
</script>

</body>
</html>